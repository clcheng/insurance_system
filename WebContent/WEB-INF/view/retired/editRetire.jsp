<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%-- <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> --%>
<html>
<head>
<title>在职转退休</title>
</head>
    <body>
        <%-- <jsp:include page="../header.jsp"/> --%>
        <%@ include file="../header.jsp" %>
        <!--主要区域开始-->
        <div id="main" style="margin: auto;">
        	<jsp:include page="../file_manage.jsp"/>
        	<!-- 加载部分 -->
			<div style="font-size:14px;margin-top:15px;margin-bottom:15px">退休职工信息</div>
	<form class="layui-form layui-form-pane" action="${pageContext.request.contextPath}/retired/editForm" method="post" style="margin:auto;">
		<div class="layui-form-item">
	    	<label class="layui-form-label">身份证号</label>
		    <div class="layui-input-inline">
		      <input type="text" name="retired_id" autocomplete="off" class="layui-input" value="${retired.retired_id}" readonly="readonly">
		    </div>
		    <label class="layui-form-label">姓名</label>
		    <div class="layui-input-inline">
		      <input name="retired_name" autocomplete="off" class="layui-input" type="text" value="${retired.retired_name}" readonly="readonly">
		    </div>
		  </div>
		<div class="layui-form-item">
	      <label class="layui-form-label">单位编号</label>
		    <div class="layui-input-inline">
		      <input name="comp_id" autocomplete="off" class="layui-input" type="text" value="${retired.comp_id}" readonly="readonly">
		    </div>
	    	<label class="layui-form-label">电话</label>
	      <div class="layui-input-inline">
	        <input name="retired_telephone" autocomplete="off" class="layui-input" type="tel" value="${retired.retired_telephone}" readonly="readonly">
	      </div>
	      </div>
		<div class="layui-form-item">
	    	<label class="layui-form-label">住址</label>
		    <div class="layui-input-inline">
		      <input name="retired_address" autocomplete="off" class="layui-input" type="text" value="${retired.retired_address}" readonly="readonly">
		    </div>
		    <label class="layui-form-label">邮编</label>
		    <div class="layui-input-inline">
		      <input name="retired_post" autocomplete="off" class="layui-input" type="text" value="${retired.retired_post}" readonly="readonly">
		    </div>
		  </div>
		<div class="layui-form-item">
			<label class="layui-form-label">性别</label>
			<div class="layui-input-inline">
		      <input name="retired_sex" autocomplete="off" class="layui-input" type="text" value="${retired.retired_sex}" readonly="readonly">
		    </div>
		    <div class="layui-inline">
		      <label class="layui-form-label">出生日期</label>
		      <div class="layui-input-inline">
		        <input name="retired_birth" class="layui-input" value="<fmt:formatDate value="${retired.retired_birth}" pattern="yyyy-MM-dd"/>" type="text" readonly="readonly">
		      </div>
		    </div>
		  </div>
		<div class="layui-form-item">
	    	<label class="layui-form-label">民族</label>
	    	<div class="layui-input-inline">
		      <input name="retired_nation" autocomplete="off" class="layui-input" type="text" value="${retired.retired_nation}" readonly="readonly">
		    </div>
	    	<label class="layui-form-label">用工形式</label>
	    	<div class="layui-input-inline">
		      <input name="retired_worktype" autocomplete="off" class="layui-input" type="text" value="${retired.retired_worktype}" readonly="readonly">
		    </div>
		  </div>
		  <div class="layui-form-item">
	    	<label class="layui-form-label">职务</label>
		    <div class="layui-input-inline">
		      <input name="retired_type" autocomplete="off" class="layui-input" type="text" value="${retired.retired_type}" readonly="readonly">
		    </div>
		    <label class="layui-form-label">参加工作日期</label>
		      <div class="layui-input-inline">
		        <input name="retired_beginwork" class="layui-input" type="text" value="<fmt:formatDate value='${retired.retired_beginwork}' pattern='yyyy-MM-dd'/>" readonly="readonly">
		      </div>
		  </div>
	    <div class="layui-form-item">
		      <label class="layui-form-label">参保日期</label>
		      <div class="layui-input-inline">
		        <input name="retired_begintime" class="layui-input" type="text" value="<fmt:formatDate value='${retired.retired_begintime}' pattern='yyyy-MM-dd'/>" readonly="readonly">
		      </div>
		    <div class="layui-inline">
		      <label class="layui-form-label">账户号</label>
		      <div class="layui-input-inline">
		        <input name="retired_accid" autocomplete="off" class="layui-input" type="text" value="${retired.retired_accid}" readonly="readonly">
		      </div>
		    </div>
	    </div>
	    <div style="font-size:14px;margin-top:15px;margin-bottom:15px">修改信息</div>
		<div class="layui-form-item">
		      <label class="layui-form-label">退休时间</label>
		      <div class="layui-input-inline">
		        <input name="retired_retirtime" id="date" lay-verify="date" placeholder="yyyy-MM-dd" class="layui-input" type="text" value="<fmt:formatDate value='${retired.retired_beginwork}' pattern='yyyy-MM-dd'/>" >
		      </div>
		    <div class="layui-inline">
		      <label class="layui-form-label">缴费年数</label>
		      <div class="layui-input-inline">
		        <input name="retired_worktime" autocomplete="off" class="layui-input" value="${retired.retired_worktime}" type="text">
		      </div>
		    </div>	
	    </div>
		<div class="layui-form-item">
		      <label class="layui-form-label">个人账户总额</label>
		      <div class="layui-input-inline">
		        <input name="retired_account" autocomplete="off" class="layui-input" value="${retired.retired_account}" type="text">
		      </div>
		    <label class="layui-form-label">个人账户剩余</label>
		      <div class="layui-input-inline">
		        <input name="retired_leftaccount" autocomplete="off" class="layui-input" value="${retired.retired_leftaccount}" type="text">
		      </div>
	    </div>
	    <div class="layui-form-item">
		    <div class="layui-input-block">
		      <button class="layui-btn" lay-submit="" lay-filter="add">立即提交</button>
		      <button type="reset" class="layui-btn layui-btn-primary">重置</button>
		    </div>
  		</div>
	</form>
        </div>
        <!--主要区域结束-->
        <jsp:include page="../footer.jsp"/>
    <script>
    layui.use('form', function(){
  	  var form = layui.form;
		  //监听提交
		  form.on('submit(add)', function(data){
			  $.ajax({
		    	    type: "post",
		    	    url: "${pageContext.request.contextPath}/retired/editForm",

		    	    data: JSON.stringify(data.field),
		    	    contentType: "application/json; charset=utf-8",//(可以)

		    	    //dataType: "string",
		    	    success: function (result) {
		    	    	if(result=='success'){
		    				layer.msg('添加成功！');
		    			}else if(result=='fail1'){
		    				layer.msg('编号已存在！');
		    			}else{
		    				layer.msg('添加失败！');
		    			}
		    	    }
		     })
			  return false;
		  });
		  });
	</script>    
    </body>
</html>
