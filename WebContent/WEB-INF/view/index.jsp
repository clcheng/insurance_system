﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
<title>主页</title>
 <link type="text/css" rel="stylesheet" media="all" href="${pageContext.request.contextPath}/styles/global.css" />
 <link type="text/css" rel="stylesheet" media="all" href="${pageContext.request.contextPath}/styles/global_color.css" />  
 <link type="text/css" rel="stylesheet" media="all" href="${pageContext.request.contextPath}/styles/layui/css/layui.css" /> 
<script src="${pageContext.request.contextPath}/styles/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/styles/layui/layui.js"></script>
<script src="${pageContext.request.contextPath}/styles/layer/layer.js"></script>
<style>
#index_navi a{width:100px;text-align: center;}
</style>
</head>
    <body class="index">
    	<div class="time" style="margin:200px auto;text-align:center;font-size:38px;color:white"></div>
       <!--导航区域开始-->
        <div id="index_navi">
	        <ul class="layui-nav layui-bg-green">
			  <li class="layui-nav-item layui-this"><a href="${pageContext.request.contextPath }/index/forward/index">主页</a></li>
			  <c:choose>
			  	<c:when test="${pri ne 2}">
			  		<li class="layui-nav-item"><a href="${pageContext.request.contextPath}/company/add">档案管理</a></li>
				  <li class="layui-nav-item"><a href="${pageContext.request.contextPath }/pay/addWage">保险金缴纳管理</a></li>
				  <li class="layui-nav-item"><a href="${pageContext.request.contextPath }/grant/monthCount">保险金发放管理</a></li>
				  <li class="layui-nav-item"><a href="${pageContext.request.contextPath }/findInfo/company">信息查询</a></li>
			  </c:when>
			  	<c:otherwise>
				  <li class="layui-nav-item"><a href="${pageContext.request.contextPath }/findInfo/common">信息查询</a></li>
			  	</c:otherwise>
			  </c:choose>
			  <c:if test="${pri eq 1}">
				  <li class="layui-nav-item"><a href="${pageContext.request.contextPath }/admin/add">系统维护</a></li>
			  </c:if>
			</ul> 
        </div>
    </body>
    <script type="text/javascript">
    showTime();
    function showTime()
    {
        var today = new Date().getHours();
        var call = null;
        if(today>=5&&today<11){
			call = "上午好";
		}else if(today>=11&&today<13){
			call = "中午好";
		}else if(today>=13&&today<18){
			call = "下午好";
		}else if(today>=18&&today<22){
			call = "晚上好";
		}else{
			call = "夜深了";
		}
        $(".time").html(call);
        /* alert("The time is: " + today.toString ()); */
        setTimeout("showTime()", 50000);
    }
    </script>
</html>
