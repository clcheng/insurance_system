<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%-- <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> --%>
<html>
<head>
<title>在职转退休</title>
</head>
    <body>
        <%-- <jsp:include page="../header.jsp"/> --%>
        <%@ include file="../header.jsp" %>
        <!--主要区域开始-->
        <div id="main" style="margin: auto;">
        	<jsp:include page="../file_manage.jsp"/>
        	<!-- 加载部分 -->
			<div style="font-size:14px;margin-top:15px;margin-bottom:15px">死亡修改</div>
	<form class="layui-form layui-form-pane" action="${pageContext.request.contextPath}/dead/addRetireForm" method="post" style="margin:auto;">
		<div class="layui-form-item">
	    	<label class="layui-form-label">身份证号</label>
		    <div class="layui-input-inline">
		      <input type="text" name="dead_id" autocomplete="off" class="layui-input" value="${dead.dead_id}">
		    </div>
		    <label class="layui-form-label">姓名</label>
		    <div class="layui-input-inline">
		      <input name="dead_name" autocomplete="off" class="layui-input" type="text" value="${dead.dead_name}">
		    </div>
		  </div>
		<div class="layui-form-item">
	      <label class="layui-form-label">单位编号</label>
		    <div class="layui-input-inline">
		      <input name="comp_id" autocomplete="off" class="layui-input" type="text" value="${dead.comp_id}">
		    </div>
		    <label class="layui-form-label">性别</label>
			<div class="layui-input-inline">
		      <input name="dead_sex" autocomplete="off" class="layui-input" type="text" value="${dead.dead_sex}">
		    </div>
	      </div>
		<div class="layui-form-item">
	    	<label class="layui-form-label">账户号</label>
		    <div class="layui-input-inline">
		      <input name="dead_accid" autocomplete="off" class="layui-input" type="text" value="${dead.dead_accid}">
		    </div>
		    <label class="layui-form-label">个人账户余额</label>
		    <div class="layui-input-inline">
		      <input name="dead_account" autocomplete="off" class="layui-input" type="text" value="${dead.dead_account}">
		    </div>
		  </div>
		<div class="layui-form-item">
			<label class="layui-form-label">待领取人身份证号</label>
			<div class="layui-input-inline">
		      <input name="dead_relaid" autocomplete="off" class="layui-input" type="text" value="${dead.dead_relaid}">
		    </div>
		    <div class="layui-inline">
		      <label class="layui-form-label">待领取人姓名</label>
		      <div class="layui-input-inline">
		        <input name="dead_relaname" class="layui-input" value="${dead.dead_relaname}" type="text">
		      </div>
		    </div>
		  </div>
		<div class="layui-form-item">
	    	<label class="layui-form-label">待领取人性别</label>
	    	<div class="layui-input-inline">
		      <input name="dead_relasex" autocomplete="off" class="layui-input" type="text" value="${dead.dead_relasex}">
		    </div>
	    	<label class="layui-form-label">待领取人电话</label>
	    	<div class="layui-input-inline">
		      <input name="dead_relaphone" autocomplete="off" class="layui-input" type="text" value="${dead.dead_relaphone}">
		    </div>
		  </div>
		  <div class="layui-form-item">
	    	<label class="layui-form-label">待领取人住址</label>
		    <div class="layui-input-inline">
		      <input name="dead_relaaddress" autocomplete="off" class="layui-input" type="text" value="${dead.dead_relaaddress}">
		    </div>
		    <label class="layui-form-label">待领取人邮编</label>
		      <div class="layui-input-inline">
		        <input name="dead_relapost" class="layui-input" type="text" value="${dead.dead_relapost}">
		      </div>
		  </div>
	    <div class="layui-form-item">
		    <div class="layui-input-block">
		      <button class="layui-btn" lay-submit="" lay-filter="add">立即提交</button>
		      <button type="reset" class="layui-btn layui-btn-primary">重置</button>
		    </div>
  		</div>
	</form>
        </div>
        <!--主要区域结束-->
        <jsp:include page="../footer.jsp"/>
    <script>
    layui.use('form', function(){
  	  var form = layui.form;
		  //监听提交
		  form.on('submit(add)', function(data){
			  $.ajax({
		    	    type: "post",
		    	    url: "${pageContext.request.contextPath}/dead/editForm",

		    	    data: JSON.stringify(data.field),
		    	    contentType: "application/json; charset=utf-8",//(可以)

		    	    //dataType: "string",
		    	    success: function (result) {
		    	    	if(result=='success'){
		    				layer.msg('修改成功！');
		    			}else if(result=='fail1'){
		    				layer.msg('编号已存在！');
		    			}else{
		    				layer.msg('修改失败！');
		    			}
		    	    }
		     })
			  return false;
		  });
		  });
	</script>    
    </body>
</html>
