<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%-- <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> --%>
<html>
<head>
<title>在职转退休</title>
</head>
    <body>
        <%-- <jsp:include page="../header.jsp"/> --%>
        <%@ include file="../header.jsp" %>
        <!--主要区域开始-->
        <div id="main" style="margin: auto;">
        	<jsp:include page="../file_manage.jsp"/>
        	<!-- 加载部分 -->
			<div style="font-size:14px;margin-top:15px;margin-bottom:15px">在职转退休</div>
	<form class="layui-form layui-form-pane" action="${pageContext.request.contextPath}/worker/addRetireForm" method="post" style="margin:auto;">
		<div class="layui-form-item">
	    	<label class="layui-form-label">身份证号</label>
		    <div class="layui-input-inline">
		      <input type="text" name="retired_id" autocomplete="off" class="layui-input" value="${worker.work_id}" readonly="readonly">
		    </div>
		    <label class="layui-form-label">姓名</label>
		    <div class="layui-input-inline">
		      <input name="retired_name" autocomplete="off" class="layui-input" type="text" value="${worker.work_name}" readonly="readonly">
		    </div>
		  </div>
		<div class="layui-form-item">
	      <label class="layui-form-label">单位编号</label>
		    <div class="layui-input-inline">
		      <input name="comp_id" autocomplete="off" class="layui-input" type="text" value="${worker.comp_id}" readonly="readonly">
		    </div>
	    	<label class="layui-form-label">电话</label>
	      <div class="layui-input-inline">
	        <input name="retired_telephone" autocomplete="off" class="layui-input" type="tel" value="${worker.work_phone}" readonly="readonly">
	      </div>
	      </div>
		<div class="layui-form-item">
	    	<label class="layui-form-label">住址</label>
		    <div class="layui-input-inline">
		      <input name="retired_address" autocomplete="off" class="layui-input" type="text" value="${worker.work_address}" readonly="readonly">
		    </div>
		    <label class="layui-form-label">邮编</label>
		    <div class="layui-input-inline">
		      <input name="retired_post" autocomplete="off" class="layui-input" type="text" value="${worker.work_post}" readonly="readonly">
		    </div>
		  </div>
		<div class="layui-form-item">
			<label class="layui-form-label">性别</label>
			<div class="layui-input-inline">
		      <input name="retired_sex" autocomplete="off" class="layui-input" type="text" value="${worker.work_sex}" readonly="readonly">
		    </div>
		    <div class="layui-inline">
		      <label class="layui-form-label">出生日期</label>
		      <div class="layui-input-inline">
		        <input name="retired_birth" class="layui-input" value="<fmt:formatDate value="${worker.work_birth}" pattern="yyyy-MM-dd"/>" type="text" readonly="readonly">
		      </div>
		    </div>
		  </div>
		<div class="layui-form-item">
	    	<label class="layui-form-label">民族</label>
	    	<div class="layui-input-inline">
		      <input name="retired_nation" autocomplete="off" class="layui-input" type="text" value="${worker.work_nation}" readonly="readonly">
		    </div>
	    	<label class="layui-form-label">用工形式</label>
	    	<div class="layui-input-inline">
		      <input name="retired_worktype" autocomplete="off" class="layui-input" type="text" value="${worker.work_worktype}" readonly="readonly">
		    </div>
		  </div>
		  <div class="layui-form-item">
	    	<label class="layui-form-label">职务</label>
		    <div class="layui-input-inline">
		      <input name="retired_type" autocomplete="off" class="layui-input" type="text" value="${worker.work_type}" readonly="readonly">
		    </div>
		    <label class="layui-form-label">参加工作日期</label>
		      <div class="layui-input-inline">
		        <input name="retired_beginwork" class="layui-input" type="text" value="<fmt:formatDate value='${worker.work_beginwork}' pattern='yyyy-MM-dd'/>" readonly="readonly">
		      </div>
		  </div>
	    <div class="layui-form-item">
		      <label class="layui-form-label">参保日期</label>
		      <div class="layui-input-inline">
		        <input name="retired_begintime" class="layui-input" type="text" value="<fmt:formatDate value='${worker.work_begintime}' pattern='yyyy-MM-dd'/>" readonly="readonly">
		      </div>
		    <div class="layui-inline">
		      <label class="layui-form-label">账户号</label>
		      <div class="layui-input-inline">
		        <input name="retired_accid" autocomplete="off" class="layui-input" type="text" value="${worker.work_accid}" readonly="readonly">
		        <input name="retired_account" hidden type="text" value="${worker.work_account}" readonly="readonly">
		      </div>
		    </div>
	    </div>
	    <div style="font-size:14px;margin-top:15px;margin-bottom:15px">在职转退休</div>
		<div class="layui-form-item">
		      <label class="layui-form-label">退休时间</label>
		      <div class="layui-input-inline">
		        <input name="retired_retirtime" id="date" lay-verify="date" placeholder="yyyy-MM-dd" class="layui-input" type="text">
		      </div>
		    <div class="layui-inline">
		      <label class="layui-form-label">缴费年数</label>
		      <div class="layui-input-inline">
		        <input name="retired_worktime" autocomplete="off" class="layui-input" type="text">
		      </div>
		    </div>
	    </div>
	    <div class="layui-form-item">
		    <div class="layui-input-block">
		      <button class="layui-btn" lay-submit="" lay-filter="add">立即提交</button>
		      <button type="reset" class="layui-btn layui-btn-primary">重置</button>
		    </div>
  		</div>
	</form>
        </div>
        <!--主要区域结束-->
        <jsp:include page="../footer.jsp"/>
    <script>
    layui.use('form', function(){
  	  var form = layui.form;
		  //监听提交
		  form.on('submit(add)', function(data){
			  $.ajax({
		    	    type: "post",
		    	    url: "${pageContext.request.contextPath}/worker/addRetireForm",

		    	    data: JSON.stringify(data.field),
		    	    contentType: "application/json; charset=utf-8",//(可以)

		    	    //dataType: "string",
		    	    success: function (result) {
		    	    	if(result=='success'){
		    				layer.msg('添加成功！');
		    			}else if(result=='fail1'){
		    				layer.msg('编号已存在！');
		    			}else{
		    				layer.msg('添加失败！');
		    			}
		    	    }
		     })
			  return false;
		  });
		  });
	</script>    
    </body>
</html>
