<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>添加在职</title>
</head>
    <body>
        <%@ include file="../header.jsp" %>
        <!--主要区域开始-->
        <div id="main" style="margin: auto;">
        	<jsp:include page="../file_manage.jsp"/>
        	<!-- 加载部分 -->
			<div style="font-size:14px;margin-top:15px;margin-bottom:15px">在职职工基本信息添加</div>
			<form class="layui-form layui-form-pane" action="${pageContext.request.contextPath}/worker/addForm" method="post" style="margin:auto;">
				<div class="layui-form-item">
			    	<label class="layui-form-label">身份证号</label>
				    <div class="layui-input-inline">
				      <input type="text" name="work_id" lay-verify="identity" placeholder="" autocomplete="off" class="layui-input">
				    </div>
				    <label class="layui-form-label">姓名</label>
				    <div class="layui-input-inline">
				      <input name="work_name" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" type="text">
				    </div>
				  </div>
				<div class="layui-form-item">
			      <label class="layui-form-label">单位编号</label>
				    <div class="layui-input-inline">
				      <input name="comp_id" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" type="text">
				    </div>
			    	<label class="layui-form-label">电话</label>
			      <div class="layui-input-inline">
			        <input name="work_phone" lay-verify="required|phone" autocomplete="off" class="layui-input" type="tel">
			      </div>
			      </div>
				<div class="layui-form-item">
			    	<label class="layui-form-label">住址</label>
				    <div class="layui-input-inline">
				      <input name="work_address" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" type="text">
				    </div>
				    <label class="layui-form-label">邮编</label>
				    <div class="layui-input-inline">
				      <input name="work_post" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" type="text">
				    </div>
				  </div>
				<div class="layui-form-item">
					<label class="layui-form-label">性别</label>
				    <div class="layui-input-inline">
				      <input type="radio" name="work_sex" value="男" title="男" checked="">
				      <input type="radio" name="work_sex" value="女" title="女">
				    </div>
				    <div class="layui-inline">
				      <label class="layui-form-label">出生日期</label>
				      <div class="layui-input-inline">
				        <input name="work_birth" id="date" lay-verify="date" placeholder="yyyy-MM-dd" class="layui-input" type="text">
				      </div>
				    </div>
				</div>
				<div class="layui-form-item">
			    	<label class="layui-form-label">民族</label>
				    <div class="layui-input-inline">
				      <select name="work_nation">
				        <option value="汉族" selected>汉族</option>
				        <option value="蒙古族">蒙古族</option>
				        <option value="回族">回族</option>
				        <option value="藏族">藏族</option>
				        <option value="维吾尔族">维吾尔族</option>
				        <option value="苗族">苗族</option>
				        <option value="壮族">壮族</option>
				        <option value="布依族">布依族</option>
				        <option value="满族">满族</option>
				        <option value="朝鲜族">朝鲜族</option>
				        <option value="其他">其他</option>
				      </select>
				    </div>
			    	<label class="layui-form-label">用工形式</label>
				    <div class="layui-input-inline">
				      <select name="work_worktype">
				        <option value="固定" selected>固定</option>
				        <option value="临时">临时</option>
				        <option value="非全职">非全职</option>
				      </select>
				    </div>
				  </div>
				  <div class="layui-form-item">
			    	<label class="layui-form-label">职务</label>
				    <div class="layui-input-inline">
				      <input name="work_type" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" type="text">
				    </div>
				    <label class="layui-form-label">参加工作日期</label>
				      <div class="layui-input-inline">
				        <input name="work_beginwork" id="date1" lay-verify="date" placeholder="yyyy-MM-dd" class="layui-input" type="text">
				      </div>
				  </div>
			    <div class="layui-form-item">
				      <label class="layui-form-label">参保日期</label>
				      <div class="layui-input-inline">
				        <input name="work_begintime" id="date2" lay-verify="date" placeholder="yyyy-MM-dd" class="layui-input" type="text">
				      </div>
				    <div class="layui-inline">
				      <label class="layui-form-label">账户号</label>
				      <div class="layui-input-inline">
				        <input name="work_accid" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" type="text">
				      </div>
				    </div>
			    </div>
			    <div class="layui-form-item">
				    <div class="layui-input-block">
				      <button class="layui-btn" lay-submit="" lay-filter="add">立即提交</button>
				      <button type="reset" class="layui-btn layui-btn-primary">重置</button>
				    </div>
		  		</div>
			</form>
        </div>
        <!--主要区域结束-->
        <jsp:include page="../footer.jsp"/>
    <script>
    layui.use('form', function(){
  	  var form = layui.form;
		  //监听提交
		  form.on('submit(add)', function(data){
			  $.ajax({
		    	    type: "post",
		    	    url: "${pageContext.request.contextPath}/worker/addForm",

		    	    data: JSON.stringify(data.field),
//		    	    data: data.field,
		    	    contentType: "application/json; charset=utf-8",//(可以)

		    	    //dataType: "string",
		    	    success: function (result) {
		    	    	if(result=='success'){
		    				layer.msg('添加成功！');
		    			}else if(result=='fail1'){
		    				layer.msg('编号已存在！');
		    			}else if(result=='fail2'){
		    				layer.msg('不存在该单位！');
		    			}else{
		    				layer.msg('添加失败！');
		    			}
		    	    }
		     });
		     /* var param = data.field;
		     $.post("${pageContext.request.contextPath}/worker/addForm",param,function(result){
		    	 if(result=='success'){
	    				layer.msg('添加成功！');
	    			}else if(result=='fail1'){
	    				layer.msg('编号已存在！');
	    			}else if(result=='fail2'){
	    				layer.msg('不存在该单位！');
	    			}else{
	    				layer.msg('添加失败！');
	    			}
		     }); */
			  return false;
		  });
		  });
	</script>    
    </body>
</html>
