<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%-- <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> --%>
<html>
<head>
<title>在职转退休</title>
</head>
    <body>
        <%-- <jsp:include page="../header.jsp"/> --%>
        <%@ include file="../header.jsp" %>
        <!--主要区域开始-->
        <div id="main" style="margin: auto;">
        	<jsp:include page="../grant_manage.jsp"/>
        	<!-- 加载部分 -->
			<div style="font-size:14px;margin-top:15px;margin-bottom:15px">一次性发放计算</div>
	<form class="layui-form layui-form-pane" action="${pageContext.request.contextPath}/retired/editForm" method="post" style="margin:auto;">
		<div class="layui-form-item">
	      <label class="layui-form-label">退休职工编号</label>
		    <div class="layui-input-inline">
		      <input name="retir_id" autocomplete="off" class="layui-input" type="text" value="${retiraccount.retir_id}" readonly="readonly">
		    </div>
	      </div>
		<div class="layui-form-item">
	      <label class="layui-form-label">发放日期</label>
		    <div class="layui-input-inline">
		      <input name="retiracc_date" autocomplete="off" class="layui-input" type="text" value="<fmt:formatDate value="${retiraccount.retiracc_date}" pattern="yyyy-MM-dd"/>" readonly="readonly">
		    </div>
	      </div>
		<div class="layui-form-item">
	      <label class="layui-form-label">所在公司编号</label>
		    <div class="layui-input-inline">
		      <input name="comp_id" autocomplete="off" class="layui-input" type="text" value="${retiraccount.comp_id}" readonly="readonly">
		    </div>
	      </div>
		<div class="layui-form-item">
	      <label class="layui-form-label">一次性发放总金额</label>
		    <div class="layui-input-inline">
		      <input name="retiracc_money" autocomplete="off" class="layui-input" type="text" value="${retiraccount.retiracc_money}" readonly="readonly">
		    </div>
	      </div>
		<div class="layui-form-item">
	      <label class="layui-form-label">领取状态</label>
		    <div class="layui-input-inline">
		      <input name="retiracc_flag" autocomplete="off" class="layui-input" type="text" value="${retiraccount.retiracc_flag eq 1?'已领取':'未领取'}" readonly="readonly">
		    </div>
	      </div>
	</form>
        </div>
        <!--主要区域结束-->
        <jsp:include page="../footer.jsp"/>
    </body>
</html>
