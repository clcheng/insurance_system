<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>记账列表</title>
</head>
    <body>
        <%@ include file="../header.jsp" %>
        <!--主要区域开始-->
        <div id="main" style="margin: auto;">
        	<jsp:include page="../grant_manage.jsp"/>
        	<!-- 加载部分 -->
			<div style="font-size:14px;margin-top:15px;margin-bottom:15px">记账列表</div>
			<table class="layui-hide" id="demo" lay-filter="table"></table>
        </div>
        <div>
        	<input hidden type="text" value="${retiracc_flag}" id="retiracc_flag">
        	<input hidden type="text" value="${retir_id}" id="retir_id">
        </div>
        <!--主要区域结束-->
        <jsp:include page="../footer.jsp"/>
    <script>
    var retiracc_flag = $("#retiracc_flag").val();;
    var retir_id = $("#retir_id").val();
layui.use('table', function(){
  var table = layui.table;
  //展示已知数据
  table.render({
    elem: '#demo'
    ,url:'${pageContext.request.contextPath}/grant/list'
    ,where:{flag:retiracc_flag,cid:retir_id}
    ,cols: [[ //标题栏
      {field: 'id', title: '记录号', width: 120, sort: true}
      ,{field: 'retir_id', title: '退休职工编号', width: 120}
      ,{field: 'retiracc_date', title: '发放日期', width: 120}
      ,{field: 'comp_id', title: '所在公司', width: 120}
      ,{field: 'retiracc_money', title: '本月发放养老金总金额', width: 120}
      ,{field: 'retiracc_flag', title: '发放标志', width: 120,templet: '#titleTpl'}
      /* ,{field: '', title: '操作', width: 360, toolbar: '#barDemo'} */
    ]]
    //,skin: 'line' //表格风格
    ,even: true
    ,page: true //是否显示分页
    ,limits: [5, 7, 10]
    ,limit: 5 //每页默认显示的数量
  });
  
  
});
</script>
<script type="text/html" id="barDemo">
  <a class="layui-btn layui-btn-xs" lay-event="edit">修改</a>
  <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</script>
<script type="text/html" id="titleTpl">
  {{#  if(d.compacc_flag < 1){ }}
    未发放
  {{#  } else if(d.compacc_flag > 1){ }}
已死亡
  {{#  } else { }}
    已发放
  {{#  } }}
</script>
    </body>
</html>
