﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html>
<head>
<title>主页</title>
 <link type="text/css" rel="stylesheet" media="all" href="${pageContext.request.contextPath}/styles/global.css" />
 <link type="text/css" rel="stylesheet" media="all" href="${pageContext.request.contextPath}/styles/global_color.css" /> 
 <link type="text/css" rel="stylesheet" media="all" href="${pageContext.request.contextPath}/styles/layui/css/layui.css" /> 
 <link type="text/css" rel="stylesheet" media="all" href="${pageContext.request.contextPath}/styles/layer/theme/default/layer.css" /> 
<script src="${pageContext.request.contextPath}/styles/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/styles/layui/layui.js"></script>
<script src="${pageContext.request.contextPath}/styles/layer/layer.js"></script>
<script type="text/javascript">
layui.use('form', function(){
  var form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
  form.render();
}); 
</script>
<style>
.content{font-size:12px;margin-left:170px;}
#navi a{width:100px;text-align: center;}
.layui-form-pane .layui-form-label {width: 137px}
</style>
</head>
    <body>
        <!--Logo区域开始-->
        <div id="header">
            <img src="${pageContext.request.contextPath}/images/logo.png" alt="logo" class="left"/>
            <a href="#">[退出]</a>            
        </div>
        <!--Logo区域结束-->
        <!--导航区域开始-->
        <div id="navi">                        
            <ul class="layui-nav layui-bg-green">
				  <li class="layui-nav-item "><a href="${pageContext.request.contextPath }/findInfo/common">个人信息</a></li>
				  <li class="layui-nav-item "><a href="${pageContext.request.contextPath }/findInfo/message">缴费信息</a></li>
				  <li class="layui-nav-item layui-this"><a href="${pageContext.request.contextPath }/findInfo/select">发放查询</a></li>
			</ul>            
        </div>
        <!--导航区域结束-->
		<div id="conment">
		<div style="font-size:14px;margin-top:15px;margin-bottom:15px">在职职工基本信息</div>
				<div class="layui-form-item">
			    	<label class="layui-form-label">身份证号</label>
				    <div class="layui-input-inline">
				      <input type="text" name="work_id" lay-verify="identity" autocomplete="off" class="layui-input" value="${worker.work_id}" readonly="readonly">
				    </div>
				    <label class="layui-form-label">姓名</label>
				    <div class="layui-input-inline">
				      <input name="work_name" lay-verify="required"  autocomplete="off" class="layui-input" type="text" value="${worker.work_name}" readonly="readonly">
				    </div>
				  </div>
				<div class="layui-form-item">
			      <label class="layui-form-label">单位编号</label>
				    <div class="layui-input-inline">
				      <input name="comp_id" lay-verify="required"  autocomplete="off" class="layui-input" type="text" value="${worker.comp_id}" readonly="readonly">
				    </div>
			    	<label class="layui-form-label">电话</label>
			      <div class="layui-input-inline">
			        <input name="work_phone" lay-verify="required|phone" autocomplete="off" class="layui-input" type="tel" value="${worker.work_phone}" readonly="readonly">
			      </div>
				  </div>
				<div class="layui-form-item">
			    	<label class="layui-form-label">住址</label>
				    <div class="layui-input-inline">
				      <input name="work_address" lay-verify="required"  autocomplete="off" class="layui-input" type="text" value="${worker.work_address}" readonly="readonly">
				    </div>
				    <label class="layui-form-label">邮编</label>
				    <div class="layui-input-inline">
				      <input name="work_post" lay-verify="required"  autocomplete="off" class="layui-input" type="text" value="${worker.work_post}" readonly="readonly">
				    </div>
				  </div>
				<div class="layui-form-item">
					<label class="layui-form-label">性别</label>
				    <div class="layui-input-inline">
				      <input type="radio" name="work_sex" value="男" title="男" checked="">
				      <input type="radio" name="work_sex" value="女" title="女">
				    </div>
				    <div class="layui-inline">
				      <label class="layui-form-label">出生日期</label>
				      <div class="layui-input-inline">
				        <input name="work_birth" id="date"  class="layui-input" type="text" value="<fmt:formatDate value="${worker.work_birth}" pattern='yyyy-MM-dd'/>" readonly="readonly">
				      </div>
				    </div>
				  </div>
				<div class="layui-form-item">
			    	<label class="layui-form-label">民族</label>
				    <div class="layui-input-inline">
				      <input name="work_nation" value="${worker.work_nation}" readonly="readonly">
				    </div>
			    	<label class="layui-form-label">用工形式</label>
				    <div class="layui-input-inline">
				      <input name="work_worktype" value="${worker.work_worktype}" readonly="readonly">
				    </div>
				  </div>
				  <div class="layui-form-item">
			    	<label class="layui-form-label">职务</label>
				    <div class="layui-input-inline">
				      <input name="work_type" lay-verify="required"  autocomplete="off" class="layui-input" type="text" value="${worker.work_type}" readonly="readonly">
				    </div>
				    <label class="layui-form-label">参加工作日期</label>
				      <div class="layui-input-inline">
				        <input name="work_beginwork" id="date"  class="layui-input" type="text" value="<fmt:formatDate value="${worker.work_beginwork}" pattern='yyyy-MM-dd'/>" readonly="readonly">
				      </div>
				  </div>
			    <div class="layui-form-item">
				      <label class="layui-form-label">参保日期</label>
				      <div class="layui-input-inline">
				        <input name="work_begintime" id="date"  class="layui-input" type="text" value="<fmt:formatDate value="${worker.work_begintime}" pattern='yyyy-MM-dd'/>" readonly="readonly">
				      </div>
				    <div class="layui-inline">
				      <label class="layui-form-label">账户号</label>
				      <div class="layui-input-inline">
				        <input name="work_accid" lay-verify="required"  autocomplete="off" class="layui-input" type="text" value="${worker.work_accid}" readonly="readonly">
				      </div>
				    </div>
			    </div>
		</div>
		<jsp:include page="../footer.jsp"/>
    </body>
</html>