﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<ul class="layui-nav layui-bg-blue" id="fa-opera">
  <li class="layui-nav-item">
    <a href="javascript:;">单位档案</a>
    <dl class="layui-nav-child">
      <dd><a href="${pageContext.request.contextPath}/company/add">添加单位</a></dd>
      <dd><a href="${pageContext.request.contextPath}/company/find">修改单位</a></dd>
	<!-- <dd class="layui-this"><a href="">修改单位</a></dd> -->
    </dl>
  </li>
  <li class="layui-nav-item">
    <a href="javascript:;">在职档案</a>
    <dl class="layui-nav-child">
      <dd><a href="${pageContext.request.contextPath}/worker/add">添加在职</a></dd>
      <dd><a href="${pageContext.request.contextPath}/worker/find">修改在职</a></dd>
      <dd><a href="${pageContext.request.contextPath}/worker/findRetire">在职转退休</a></dd>
      <dd><a href="${pageContext.request.contextPath}/worker/findDead">在职转死亡</a></dd>
    </dl>
  </li>
  <li class="layui-nav-item">
    <a href="javascript:;">退休档案</a>
    <dl class="layui-nav-child">
      <dd><a href="${pageContext.request.contextPath}/retired/findRetire">修改退休</a></dd>
      <dd><a href="${pageContext.request.contextPath}/retired/findRetireDead">退休转死亡</a></dd>
    </dl>
  </li>
  <li class="layui-nav-item">
    <a href="javascript:;">死亡档案</a>
    <dl class="layui-nav-child">
      <dd><a href="${pageContext.request.contextPath}/dead/findDead">死亡信息修改</a></dd>
    </dl>
  </li>
</ul>