<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>工资录入</title>
</head>
    <body>
        <%@ include file="../header.jsp" %>
        <!--主要区域开始-->
        <div id="main" style="margin: auto;">
        	<jsp:include page="../pay_manage.jsp"/>
        	<!-- 加载部分 -->
			<div style="font-size:14px;margin-top:15px;margin-bottom:15px">工资录入</div>
			<form class="layui-form layui-form-pane" action="${pageContext.request.contextPath}/admin/addForm" method="post" style="margin:auto;">
				<div class="layui-form-item">
			    	<label class="layui-form-label">身份证号</label>
				    <div class="layui-input-inline">
				      <input type="text" name="work_id" lay-verify="identity" placeholder="请输入" autocomplete="off" class="layui-input">
				    </div>
				  </div>
			      <div class="layui-form-item">
				    <label class="layui-form-label">工资</label>
				    <div class="layui-input-inline">
				      <input name="work_salary" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" type="text">
				    </div>
				  </div>
			      <div class="layui-form-item">
				    <label class="layui-form-label">所属公司编号</label>
				    <div class="layui-input-inline">
				      <input type="text" name="comp_id" placeholder="请输入" lay-verify="required" autocomplete="off" class="layui-input">
				    </div>
				  </div>
			    <div class="layui-form-item">
				    <div class="layui-input-block">
				      <button class="layui-btn" lay-submit="" lay-filter="add">立即提交</button>
				      <button type="reset" class="layui-btn layui-btn-primary">重置</button>
				    </div>
		  		</div>
			</form>
        </div>
        <!--主要区域结束-->
        <jsp:include page="../footer.jsp"/>
    <script>
    layui.use('form', function(){
  	  var form = layui.form;
		  //监听提交
		  form.on('submit(add)', function(data){
			  $.ajax({
		    	    type: "post",
		    	    url: "${pageContext.request.contextPath}/pay/addForm",

		    	    data: JSON.stringify(data.field),
//		    	    data: data.field,
		    	    contentType: "application/json; charset=utf-8",//(可以)

		    	    //dataType: "string",
		    	    success: function (result) {
		    	    	if(result=='success'){
		    				layer.msg('添加成功！');
		    			}else if(result=='fail1'){
		    				layer.msg('编号已存在！');
		    			}else if(result=='fail2'){
		    				layer.msg('不存在该单位！');
		    			}else{
		    				layer.msg('添加失败！');
		    			}
		    	    }
		     });
			  return false;
		  });
		  });
	</script>    
    </body>
</html>
