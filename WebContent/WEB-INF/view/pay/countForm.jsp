<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%-- <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> --%>
<html>
<head>
<title></title>
</head>
    <body>
        <%-- <jsp:include page="../header.jsp"/> --%>
        <%@ include file="../header.jsp" %>
        <!--主要区域开始-->
        <div id="main" style="margin: auto;">
        	<jsp:include page="../pay_manage.jsp"/>
        	<!-- 加载部分 -->
			<div style="font-size:14px;margin-top:15px;margin-bottom:15px"></div>
	<form class="layui-form layui-form-pane" action="${pageContext.request.contextPath}/retired/editForm" method="post" style="margin:auto;">
		<div class="layui-form-item">
	      <label class="layui-form-label">单位编号</label>
		    <div class="layui-input-inline">
		      <input name="comp_id" autocomplete="off" class="layui-input" type="text" value="${compaccount.comp_id}" readonly="readonly">
		    </div>
	      </div>
		<div class="layui-form-item">
	      <label class="layui-form-label">缴费日期</label>
		    <div class="layui-input-inline">
		      <input name="compacc_date" autocomplete="off" class="layui-input" type="text" value="<fmt:formatDate value="${compaccount.compacc_date}" pattern="yyyy-MM-dd"/>" readonly="readonly">
		    </div>
	      </div>
		<div class="layui-form-item">
	      <label class="layui-form-label">个人缴费总金额</label>
		    <div class="layui-input-inline">
		      <input name="workacc_totalmoney" autocomplete="off" class="layui-input" type="text" value="${compaccount.workacc_totalmoney}" readonly="readonly">
		    </div>
	      </div>
		<div class="layui-form-item">
	      <label class="layui-form-label">单位缴费总金额</label>
		    <div class="layui-input-inline">
		      <input name="compacc_totalmoney" autocomplete="off" class="layui-input" type="text" value="${compaccount.compacc_totalmoney}" readonly="readonly">
		    </div>
	      </div>
		<div class="layui-form-item">
	      <label class="layui-form-label">单位补缴费用</label>
		    <div class="layui-input-inline">
		      <input name="compacc_latermoney" autocomplete="off" class="layui-input" type="text" value="${compaccount.compacc_latermoney}" readonly="readonly">
		    </div>
	      </div>
		<div class="layui-form-item">
	      <label class="layui-form-label">缴费标志</label>
		    <div class="layui-input-inline">
		      <input name="compaccFlag" autocomplete="off" class="layui-input" type="text" value="${compaccount.compacc_flag eq 1?'已缴费':'延迟缴费'}" readonly="readonly">
		    </div>
	      </div>
	</form>
        </div>
        <!--主要区域结束-->
        <jsp:include page="../footer.jsp"/>
    </body>
</html>
