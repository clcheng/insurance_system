﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html>
<head>
<title>主页</title>
 <link type="text/css" rel="stylesheet" media="all" href="${pageContext.request.contextPath}/styles/global.css" />
 <link type="text/css" rel="stylesheet" media="all" href="${pageContext.request.contextPath}/styles/global_color.css" /> 
 <link type="text/css" rel="stylesheet" media="all" href="${pageContext.request.contextPath}/styles/layui/css/layui.css" /> 
 <link type="text/css" rel="stylesheet" media="all" href="${pageContext.request.contextPath}/styles/layer/theme/default/layer.css" /> 
<script src="${pageContext.request.contextPath}/styles/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/styles/layui/layui.js"></script>
<script src="${pageContext.request.contextPath}/styles/layer/layer.js"></script>
<script type="text/javascript">
layui.use('form', function(){
  var form = layui.form; //只有执行了这一步，部分表单元素才会自动修饰成功
  form.render();
}); 
</script>
<style>
.content{font-size:12px;margin-left:170px;}
#navi a{width:100px;text-align: center;}
.layui-form-pane .layui-form-label {width: 137px}
</style>
</head>
    <body>
        <!--Logo区域开始-->
        <div id="header">
            <img src="${pageContext.request.contextPath}/images/logo.png" alt="logo" class="left"/>
            <a href="#">[退出]</a>            
        </div>
        <!--Logo区域结束-->
        <!--导航区域开始-->
        <div id="navi">                        
            <ul class="layui-nav layui-bg-green">
			  <li class="layui-nav-item"><a href="${pageContext.request.contextPath }/index/forward/index">主页</a></li>
			  <c:choose>
			  	<c:when test="${pri ne 2}">
				  <li class="layui-nav-item ${fn:contains(pageContext.request.requestURL,'/company/')||fn:contains(pageContext.request.requestURL,'worker')
				  ||fn:contains(pageContext.request.requestURL,'/retired/')||fn:contains(pageContext.request.requestURL,'/dead/')?'layui-this':''}"><a href="${pageContext.request.contextPath}/company/add">档案管理</a></li>
				  <li class="layui-nav-item ${fn:contains(pageContext.request.requestURL,'/pay/')?'layui-this':''}"><a href="${pageContext.request.contextPath }/pay/addWage">保险金缴纳管理</a></li>
				  <li class="layui-nav-item ${fn:contains(pageContext.request.requestURL,'/grant/')?'layui-this':''}"><a href="${pageContext.request.contextPath }/grant/monthCount">保险金发放管理</a></li>
				  <li class="layui-nav-item ${fn:contains(pageContext.request.requestURL,'/findInfo/')?'layui-this':''}"><a href="${pageContext.request.contextPath }/findInfo/company">信息查询</a></li>
			  	</c:when>
			  	<c:otherwise>
				  	<li class="layui-nav-item ${fn:contains(pageContext.request.requestURL,'/findInfo/')?'layui-this':''}"><a href="${pageContext.request.contextPath }/findInfo/company">信息查询</a></li>
			  	</c:otherwise>
			  </c:choose>
			  <c:if test="${pri eq 1}">
			  <li class="layui-nav-item ${fn:contains(pageContext.request.requestURL,'/admin/')?'layui-this':''}"><a href="${pageContext.request.contextPath }/admin/add">系统维护</a></li>
			  </c:if>
			</ul>            
        </div>
        <!--导航区域结束-->
    </body>
</html>
