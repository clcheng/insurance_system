﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<ul class="layui-nav layui-bg-blue" id="fa-opera">
  <li class="layui-nav-item">
    <a href="javascript:;">工资录入</a>
    <dl class="layui-nav-child">
      <dd><a href="${pageContext.request.contextPath}/pay/addWage">录入工资</a></dd>
    </dl>
  </li>
  <li class="layui-nav-item">
    <a href="javascript:;">缴纳</a>
    <dl class="layui-nav-child">
      <dd><a href="${pageContext.request.contextPath}/pay/payCount">缴纳计算</a></dd>
      <dd><a href="${pageContext.request.contextPath}/pay/payTally">缴纳记账</a></dd>
      <dd><a href="${pageContext.request.contextPath}/pay/payAfter">补交记账</a></dd>
  </li>
</ul>