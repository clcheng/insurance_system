<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>登录</title>
 <link type="text/css" rel="stylesheet" media="all" href="${pageContext.request.contextPath}/styles/global.css" />
 <link type="text/css" rel="stylesheet" media="all" href="${pageContext.request.contextPath}/styles/global_color.css" /> 
<script src="${pageContext.request.contextPath}/styles/jquery.min.js"></script>
<script>
	function login(){
		$("form").submit();
	}
</script>
</head>
    <body class="index">
        <div class="login_box">
        <form action="${pageContext.request.contextPath}/admin/login" method="post">
            <table>
                <tr>
                    <td class="login_info">账号：</td>
                    <td colspan="2"><input name="userid" type="text" class="width150" /></td>
                    <td class="login_error_info"><span class="required"></span></td>
                </tr>
                <tr>
                    <td class="login_info">密码：</td>
                    <td colspan="2"><input name="password" type="password" class="width150" /></td>
                    <td><span class="required"></span></td>
                </tr>
                <%-- <tr>
                    <td class="login_info">验证码：</td>
                    <td class="width70"><input name="" type="text" class="width70" /></td>
                    <td> <img src="${pageContext.request.contextPath}/images/valicode.jpg" alt="验证码" title="点击更换" /> </td> 
                   <!--  <td><span class="required">验证码错误</span></td>    -->           
                </tr>   --%>          
                <tr>
                    <td></td>
                    <td class="login_button" colspan="2">
                        <a href="#" onclick="login()"><img src="${pageContext.request.contextPath}/images/login_btn.png" /></a>
                    </td>    
                    <td><span class="required">${error }</span></td>                
                </tr>
            </table>
           </form>
        </div>
</body>
</html>