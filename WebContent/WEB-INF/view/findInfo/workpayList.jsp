<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>记账列表</title>
</head>
    <body>
        <%@ include file="../header.jsp" %>
        <!--主要区域开始-->
        <div id="main" style="margin: auto;">
        	<jsp:include page="../information_manage.jsp"/>
        	<!-- 加载部分 -->
			<div style="font-size:14px;margin-top:15px;margin-bottom:15px">记账列表</div>
			<table class="layui-hide" id="demo" lay-filter="table"></table>
        </div>
        <div>
      		<input hidden type="text" value="${workacc_flag}" id="workacc_flag">
        	<input hidden type="text" value="${work_id}" id="work_id">
        </div>
        <!--主要区域结束-->
        <jsp:include page="../footer.jsp"/>
    <script>
    var workacc_flag = $("#workacc_flag").val();;
    var work_id = $("#work_id").val();
layui.use('table', function(){
  var table = layui.table;
  //展示已知数据
  table.render({
    elem: '#demo'
    ,url:'${pageContext.request.contextPath}/findInfo/wplist'
    ,where:{cid:work_id}
    ,cols: [[ //标题栏
      {field: 'id', title: '记录号', width: 120, sort: true}
      ,{field: 'comp_id', title: '公司编号', width: 120}
      ,{field: 'workacc_date', title: '缴费日期', width: 120,templet: '#createTime'}
      ,{field: 'work_salary', title: '本月工资', width: 120}
      ,{field: 'workacc_salary', title: '本月缴费工资基数', width: 120}
      ,{field: 'workacc_money', title: '职工缴费', width: 120}
      ,{field: 'compacc_money', title: '单位缴费', width: 120}
      ,{field: 'compacc_flag', title: '缴费标志', width: 120,templet: '#titleTpl'}
      /* ,{field: '', title: '操作', width: 360, toolbar: '#barDemo'} */
    ]]
    //,skin: 'line' //表格风格
    ,even: true
    ,page: true //是否显示分页
    ,limits: [5, 7, 10]
    ,limit: 5 //每页默认显示的数量
  });
  
  
  
  
  
//监听工具条
  table.on('tool(table)', function(obj){
    var data = obj.data;
    if(obj.event === 'del'){
      layer.confirm('真的删除行么', function(index){
        $.ajax({
        	url:'${pageContext.request.contextPath}/admin/delete',
        	type: "POST",
            data:{"userid":data.userid},
            dataType: "json",
            success: function(data){
                if(data=="success"){
                   //删除这一行
                    obj.del();
                   //关闭弹框
                    layer.close(index);
                    layer.msg("删除成功", {icon: 6});
                }else{
                    layer.msg("删除失败", {icon: 5});
                }
            }
        });
      });
    } else if(obj.event === 'edit'){
    	window.location.href = "${pageContext.request.contextPath}/admin/editAdmin?adminId="+data.userid;
    }
  });
});
</script>
<script type="text/html" id="barDemo">
  <a class="layui-btn layui-btn-xs" lay-event="edit">修改</a>
  <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</script>
<script type="text/html" id="titleTpl">
  {{#  if(d.compacc_flag < 1){ }}
    延迟缴费
  {{#  } else { }}
    已缴费
  {{#  } }}
</script>
<script id="createTime" type="text/html">  
    {{createTime(d.workacc_date)}}   
</script>
<script type="text/javascript">  
function createTime(v){  
    var date = new Date();  
    date.setTime(v);  
    var y = date.getFullYear();  
    var m = date.getMonth()+1;  
    m = m<10?'0'+m:m;  
    var d = date.getDate();  
    d = d<10?("0"+d):d;  
    var h = date.getHours();  
    h = h<10?("0"+h):h;  
    var M = date.getMinutes();  
    M = M<10?("0"+M):M;  
    var str = y+"-"+m+"-"+d+" "+h+":"+M;  
    return str;  
}  
</script>
    </body>
</html>
