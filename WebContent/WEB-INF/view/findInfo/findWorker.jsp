<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>查询在职职工</title>
</head>
    <body>
        <%@ include file="../header.jsp" %>
        <!--主要区域开始-->
        <div id="main" style="margin: auto;">
        	<jsp:include page="../information_manage.jsp"/>
			<div style="font-size:14px;margin-top:15px;margin-bottom:15px">在职职工基本信息查询</div>
			<form class="layui-form layui-form-pane" method="post" style="margin:auto;">
				<div class="layui-form-item">
			    	<label class="layui-form-label">职工身份证号</label>
				    <div class="layui-input-inline">
				      <input id="workId" name="work_id" lay-verify="required" placeholder="请输入要修改的身份证号" autocomplete="off" class="layui-input" type="text">
				    </div>
				  </div>
				  <div class="layui-form-item">
				    <div class="layui-input-block">
				      <button class="layui-btn" lay-submit="" lay-filter="find">立即提交</button>
				      <button type="reset" class="layui-btn layui-btn-primary">重置</button>
				    </div>
		  		</div>
			</form>
        </div>
        <!--主要区域结束-->
        <jsp:include page="../footer.jsp"/>
    <script>
    layui.use('form', function(){
  	  var form = layui.form;
		  //监听提交
		  form.on('submit(find)', function(data){
			  var workId = $("#workId").val();
			     $.ajax({
			    	    type: "post",
			    	    url: "${pageContext.request.contextPath}/worker/findForm",

			    	    data: data.field,
			    	    //contentType: "application/json; charset=utf-8",//(可以)

			    	    //dataType: "string",
			    	    success: function (result) {
			    	    	if(result=='fail'){
			    				layer.msg('查询失败！');
			    			}else if(result=='fail1'){
			    				layer.msg('编号不存在！');
			    			}else{
			    				window.location.href = "${pageContext.request.contextPath}/findInfo/showWorker?workId="+workId;
			    			}
			    	    }
			     })
			  return false;
		  });
		  });
	</script>    
    </body>
</html>
