<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>修改在职</title>
</head>
    <body>
        <%@ include file="../header.jsp" %>
        <!--主要区域开始-->
        <div id="main" style="margin: auto;">
        	<jsp:include page="../information_manage.jsp"/>
        	<!-- 加载部分 -->
			<div style="font-size:14px;margin-top:15px;margin-bottom:15px">在职职工基本信息</div>
			<form class="layui-form layui-form-pane" action="${pageContext.request.contextPath}/worker/editForm" method="post" style="margin:auto;">
				<div class="layui-form-item">
			    	<label class="layui-form-label">身份证号</label>
				    <div class="layui-input-inline">
				      <input type="text" name="work_id" lay-verify="identity" autocomplete="off" class="layui-input" value="${worker.work_id}" readonly="readonly">
				    </div>
				    <label class="layui-form-label">姓名</label>
				    <div class="layui-input-inline">
				      <input name="work_name" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" type="text" value="${worker.work_name}" readonly="readonly">
				    </div>
				  </div>
				<div class="layui-form-item">
			      <label class="layui-form-label">单位编号</label>
				    <div class="layui-input-inline">
				      <input name="comp_id" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" type="text" value="${worker.comp_id}" readonly="readonly">
				    </div>
			    	<label class="layui-form-label">电话</label>
			      <div class="layui-input-inline">
			        <input name="work_phone" lay-verify="required|phone" autocomplete="off" class="layui-input" type="tel" value="${worker.work_phone}" readonly="readonly">
			      </div>
				  </div>
				<div class="layui-form-item">
			    	<label class="layui-form-label">住址</label>
				    <div class="layui-input-inline">
				      <input name="work_address" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" type="text" value="${worker.work_address}" readonly="readonly">
				    </div>
				    <label class="layui-form-label">邮编</label>
				    <div class="layui-input-inline">
				      <input name="work_post" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" type="text" value="${worker.work_post}" readonly="readonly">
				    </div>
				  </div>
				<div class="layui-form-item">
					<label class="layui-form-label">性别</label>
				    <div class="layui-input-inline">
				      <input type="radio" name="work_sex" value="男" title="男" checked="">
				      <input type="radio" name="work_sex" value="女" title="女">
				    </div>
				    <div class="layui-inline">
				      <label class="layui-form-label">出生日期</label>
				      <div class="layui-input-inline">
				        <input name="work_birth" id="date" placeholder="yyyy-MM-dd" class="layui-input" type="text" value="<fmt:formatDate value="${worker.work_birth}" pattern='yyyy-MM-dd'/>" readonly="readonly">
				      </div>
				    </div>
				  </div>
				<div class="layui-form-item">
			    	<label class="layui-form-label">民族</label>
				    <div class="layui-input-inline">
				      <input name="work_nation" value="${worker.work_nation}" readonly="readonly">
				    </div>
			    	<label class="layui-form-label">用工形式</label>
				    <div class="layui-input-inline">
				      <input name="work_worktype" value="${worker.work_worktype}" readonly="readonly">
				    </div>
				  </div>
				  <div class="layui-form-item">
			    	<label class="layui-form-label">职务</label>
				    <div class="layui-input-inline">
				      <input name="work_type" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" type="text" value="${worker.work_type}" readonly="readonly">
				    </div>
				    <label class="layui-form-label">参加工作日期</label>
				      <div class="layui-input-inline">
				        <input name="work_beginwork" id="date" placeholder="yyyy-MM-dd" class="layui-input" type="text" value="<fmt:formatDate value="${worker.work_beginwork}" pattern='yyyy-MM-dd'/>" readonly="readonly">
				      </div>
				  </div>
			    <div class="layui-form-item">
				      <label class="layui-form-label">参保日期</label>
				      <div class="layui-input-inline">
				        <input name="work_begintime" id="date" placeholder="yyyy-MM-dd" class="layui-input" type="text" value="<fmt:formatDate value="${worker.work_begintime}" pattern='yyyy-MM-dd'/>" readonly="readonly">
				      </div>
				    <div class="layui-inline">
				      <label class="layui-form-label">账户号</label>
				      <div class="layui-input-inline">
				        <input name="work_accid" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" type="text" value="${worker.work_accid}" readonly="readonly">
				      </div>
				    </div>
			    </div>
			    <!-- <div class="layui-form-item">
				    <div class="layui-input-block">
				      <button class="layui-btn" lay-submit="" lay-filter="add">立即提交</button>
				      <button type="reset" class="layui-btn layui-btn-primary">重置</button>
				    </div>
		  		</div> -->
			</form>
        </div>
        <!--主要区域结束-->
        <jsp:include page="../footer.jsp"/>
    </body>
</html>
