<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>查询退休员工</title>
</head>
    <body>
        <%@ include file="../header.jsp" %>
        <!--主要区域开始-->
        <div id="main" style="margin: auto;">
        	<jsp:include page="../information_manage.jsp"/>
			<div style="font-size:14px;margin-top:15px;margin-bottom:15px">退休职工查询</div>
			<form class="layui-form layui-form-pane" method="post" style="margin:auto;">
				<div class="layui-form-item">
			    	<label class="layui-form-label">职工身份证号</label>
				    <div class="layui-input-inline">
				      <input id="deadId" name="dead_id" lay-verify="required" placeholder="请输入要修改的身份证号" autocomplete="off" class="layui-input" type="text">
				    </div>
				  </div>
				  <div class="layui-form-item">
				    <div class="layui-input-block">
				      <button class="layui-btn" lay-submit="" lay-filter="find">立即提交</button>
				      <button type="reset" class="layui-btn layui-btn-primary">重置</button>
				    </div>
		  		</div>
			</form>
        </div>
        <!--主要区域结束-->
        <jsp:include page="../footer.jsp"/>
    <script>
    layui.use('form', function(){
    	  var form = layui.form;
		  //监听提交
		  form.on('submit(find)', function(data){
			  debugger;
			  var deadId = $("#deadId").val();
			     $.ajax({
			    	    type: "post",
			    	    url: "${pageContext.request.contextPath}/dead/findForm",

			    	    data: data.field,
			    	    //contentType: "application/json; charset=utf-8",//(可以)
			    	    //dataType: "string",
			    	    success: function (result) {
			    	    	if(result=='fail'){
			    				layer.msg('查询失败！');
			    			}else if(result=='fail1'){
			    				layer.msg('编号不存在！');
			    			}else{
			    				/* $.post('${pageContext.request.contextPath}/worker/retire?workId='+workId,function(data){
			    					$(".cont").html(data);
			    				}); */
			    				window.location.href = "${pageContext.request.contextPath}/findInfo/showDead?deadId="+deadId;
			    			}
			    	    }
			     })
			  return false;
		  });
    });
	</script>    
    </body>
</html>
