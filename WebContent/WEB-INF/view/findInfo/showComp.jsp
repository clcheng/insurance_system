<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>修改企业</title>
<style type="text/css">
.a1{
 margin-left:140px;margin-top:15px;
 color:black;
 font-size:18px;
}
</style>
</head>
    <body>
        <%@ include file="../header.jsp" %>
        <!--主要区域开始-->
        <div id="main" style="margin: auto;">
        	<jsp:include page="../information_manage.jsp"/>
			<div style="font-size:14px;margin-top:15px;margin-bottom:15px">单位信息</div>
	<form class="layui-form layui-form-pane" action="${pageContext.request.contextPath}/company/addForm" method="post" style="margin:auto;">
		<div class="layui-form-item">
	    	<label class="layui-form-label">单位编号</label>
		    <div class="layui-input-inline">
		      <input name="comp_id" value="${company.comp_id }" autocomplete="off" class="layui-input" type="text" readonly="readonly">
		    </div>
		    <label class="layui-form-label">单位名称</label>
		    <div class="layui-input-inline">
		      <input name="comp_name" value="${company.comp_name }" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" type="text" readonly="readonly">
		    </div>
		  </div>
		<div class="layui-form-item">
	    	<label class="layui-form-label">单位电话</label>
	      <div class="layui-input-inline">
	        <input name="comp_phone" value="${company.comp_phone }" lay-verify="required|phone" autocomplete="off" class="layui-input" type="tel" readonly="readonly">
	      </div>
	      <label class="layui-form-label">单位地址</label>
		    <div class="layui-input-inline">
		      <input name="comp_address" value="${company.comp_address }" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" type="text"  readonly="readonly">
		    </div>
		  </div>
		<div class="layui-form-item">
	    	<label class="layui-form-label">单位邮编</label>
		    <div class="layui-input-inline">
		      <input name="comp_post" value="${company.comp_post }" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" type="text"  readonly="readonly">
		    </div>
		    <label class="layui-form-label">法定代表</label>
		    <div class="layui-input-inline">
		      <input name="comp_law" value="${company.comp_law }" autocomplete="off" class="layui-input" type="text" readonly="readonly">
		    </div>
		  </div>
		<div class="layui-form-item">
		    <label class="layui-form-label">单位类型</label>
		    <div class="layui-input-inline">
		       <input name="comp_type" value="${company.comp_type}" class="layui-input" readonly="readonly"/>
		    </div>
		    <label class="layui-form-label">法定代表证件号</label>
		    <div class="layui-input-inline">
		      <input name="comp_lawid" value="${company.comp_lawid}" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" type="text" readonly="readonly">
		    </div>
		  </div>
		<div class="layui-form-item">
	    	<label class="layui-form-label">单位所在区县 </label>
	      <div class="layui-input-inline">
	        <input name="comp_piece" value="${company.comp_piece}" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" type="text"  readonly="readonly">
	      </div>
	      <label class="layui-form-label">单位账户号 </label>
	      <div class="layui-input-inline">
	        <input name="comp_accid" value="${company.comp_accid}" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" type="text"  readonly="readonly">
	      </div>
		  </div>
	    <div class="layui-form-item">
	      <div class="layui-inline">
		      <label class="layui-form-label">单位参保日期</label>
		      <div class="layui-input-inline">
		        <input name="com_date" lay-verify="required" class="layui-input" type="text"  value="<fmt:formatDate value='${company.com_date}' pattern='yyyy-MM-dd'/>"  readonly="readonly">
		      </div>
		    </div>
		    <div class="layui-inline">
		      <label class="layui-form-label">缴费比率</label>
		      <div class="layui-input-inline">
		        <input name="com_ratio" value="${company.com_ratio}" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" type="text"   readonly="readonly">
		      </div>
		      <div class="layui-form-mid layui-word-aux">%</div>
		    </div>
	    </div>
	    <div class="layui-form-item">
	      <label class="layui-form-label">单位账户剩余</label>
	      <div class="layui-input-inline">
	        <input name="comp_leftaccount" value="${company.comp_leftaccount}" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" type="text"   readonly="readonly">
	      </div>
        <div class="layui-form-mid layui-word-aux">元</div>
	    </div>
	</form>
        </div>
        <!--主要区域结束-->
        <jsp:include page="../footer.jsp"/>
    </body>
</html>
