<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>添加企业</title>
</head>
    <body>
        <%@ include file="../header.jsp" %>
        <!--主要区域开始-->
        <div id="main" style="margin: auto;">
        	<jsp:include page="../file_manage.jsp"/>
			<div style="font-size:14px;margin-top:15px;margin-bottom:15px">单位基本信息添加</div>
			<form class="layui-form layui-form-pane" action="${pageContext.request.contextPath}/company/addForm" method="post" style="margin:auto;">
				<div class="layui-form-item">
			    	<label class="layui-form-label">单位编号</label>
				    <div class="layui-input-inline">
				      <input name="comp_id"
				       lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" type="text">
				    </div>
				    <label class="layui-form-label">单位名称</label>
				    <div class="layui-input-inline">
				      <input name="comp_name" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" type="text">
				    </div>
				  </div>
				<div class="layui-form-item">
			    	<label class="layui-form-label">单位电话</label>
			      <div class="layui-input-inline">
			        <input name="comp_phone" lay-verify="required|phone" autocomplete="off" class="layui-input" type="tel">
			      </div>
			      <label class="layui-form-label">单位地址</label>
				    <div class="layui-input-inline">
				      <input name="comp_address" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" type="text">
				    </div>
				  </div>
				<div class="layui-form-item">
			    	<label class="layui-form-label">单位邮编</label>
				    <div class="layui-input-inline">
				      <input name="comp_post" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" type="text">
				    </div>
				    <label class="layui-form-label">法定代表</label>
				    <div class="layui-input-inline">
				      <input name="comp_law" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" type="text">
				    </div>
				  </div>
				<div class="layui-form-item">
					<div>
				    <label class="layui-form-label">单位类型</label>
				    <div class="layui-input-inline">
				      <select name="comp_type">
				        <option value="国有企业" selected>国有企业</option>
				        <option value="国有控股企业">国有控股企业</option>
				        <option value="外资企业">外资企业</option>
				        <option value="合资企业">合资企业</option>
				        <option value="私营企业">私营企业</option>
				        <option value="事业单位">事业单位</option>
				        <option value="国家行政机关">国家行政机关</option>
				        <option value="政府">政府</option>
				      </select>
				    </div></div>
				    <label class="layui-form-label">法定代表证件号</label>
				    <div class="layui-input-inline">
				      <input name="comp_lawid" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" type="text">
				    </div>
				  </div>
				<div class="layui-form-item">
			    	<label class="layui-form-label">单位所在区县 </label>
			      <div class="layui-input-inline">
			        <input name="comp_piece" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" type="text">
			      </div>
			      <label class="layui-form-label">单位账户号 </label>
			      <div class="layui-input-inline">
			        <input name="comp_accid" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" type="text">
			      </div>
				  </div>
			    <div class="layui-form-item">
			      <div class="layui-inline">
				      <label class="layui-form-label">单位参保日期</label>
				      <div class="layui-input-inline">
				        <input name="com_date" id="date" lay-verify="date" placeholder="yyyy-MM-dd" class="layui-input" type="text">
				      </div>
				    </div>
				    <div class="layui-inline">
				      <label class="layui-form-label">缴费比率</label>
				      <div class="layui-input-inline">
				        <input name="com_ratio" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" type="text">
				      </div>
				      <div class="layui-form-mid layui-word-aux">%</div>
				    </div>
			    </div>
			    <div class="layui-form-item">
			      <label class="layui-form-label">单位账户剩余</label>
			      <div class="layui-input-inline">
			        <input name="comp_leftaccount" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" type="text">
			      </div>
		        <div class="layui-form-mid layui-word-aux">元</div>
			    </div>
			    <div class="layui-form-item">
				    <div class="layui-input-block">
				      <button class="layui-btn" lay-submit="" lay-filter="add">立即提交</button>
				      <button type="reset" class="layui-btn layui-btn-primary">重置</button>
				    </div>
		  		</div>
			</form>
        </div>
        <!--主要区域结束-->
        <jsp:include page="../footer.jsp"/>
    <script>
    layui.use('form', function(){
  	  var form = layui.form;
		  //监听提交
		  form.on('submit(add)', function(data){
			  debugger;
			     $.ajax({
			    	    type: "post",
			    	    url: "${pageContext.request.contextPath}/company/addForm",
			    	    data: JSON.stringify(data.field),
			    	    contentType: "application/json; charset=utf-8",//(可以)
			    	    //dataType: "string",
			    	    success: function (result) {
			    	    	if(result=='success'){
			    				layer.msg('添加成功！');
			    			}else if(result=='fail1'){
			    				layer.msg('编号已存在！');
			    			}else{
			    				layer.msg('添加失败！');
			    			}
			    	    }
			     })
			  return false;
		  });
		  });
	</script>    
    </body>
</html>
