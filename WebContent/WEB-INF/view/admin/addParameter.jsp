<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>添加操作员</title>
</head>
    <body>
        <%@ include file="../header.jsp" %>
        <!--主要区域开始-->
        <div id="main" style="margin: auto;">
        	<jsp:include page="../system_maintenance.jsp"/>
        	<!-- 加载部分 -->
			<div style="font-size:14px;margin-top:15px;margin-bottom:15px">添加操作员</div>
			<form class="layui-form layui-form-pane" action="${pageContext.request.contextPath}/admin/addForm" method="post" style="margin:auto;">
				<div class="layui-form-item">
			    	<label class="layui-form-label">年度</label>
				    <div class="layui-input-inline">
				      <input type="text" name="year" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input">
				    </div>
				  </div>
			      <div class="layui-form-item">
				    <label class="layui-form-label">上一年度月平均工资</label>
				    <div class="layui-input-inline">
				      <input name="averagesalary" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" type="text">
				    </div>
				  </div>
				<div class="layui-form-item">
			    	<label class="layui-form-label">社会统筹基金</label>
				    <div class="layui-input-inline">
				      <input type="text" name="totalmoney" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input">
				    </div>
				  </div>
			      <div class="layui-form-item">
				    <label class="layui-form-label">个人缴费比率</label>
				    <div class="layui-input-inline">
				      <input name="work_ratio" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" type="text">
				    </div>
				  </div>
				<div class="layui-form-item">
			    	<label class="layui-form-label">划入个人账户比率</label>
				    <div class="layui-input-inline">
				      <input type="text" name="workacc_ratio" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input">
				    </div>
				  </div>
			      <div class="layui-form-item">
				    <label class="layui-form-label">单位补缴滞纳金比率</label>
				    <div class="layui-input-inline">
				      <input name="comp_lateratio" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" type="text">
				    </div>
				  </div>
			    <div class="layui-form-item">
				    <div class="layui-input-block">
				      <button class="layui-btn" lay-submit="" lay-filter="add">提交</button>
				      <button type="reset" class="layui-btn layui-btn-primary">重置</button>
				    </div>
		  		</div>
			</form>
        </div>
        <!--主要区域结束-->
        <jsp:include page="../footer.jsp"/>
    <script>
    layui.use('form', function(){
  	  var form = layui.form;
		  //监听提交
		  form.on('submit(add)', function(data){
			  $.ajax({
		    	    type: "post",
		    	    url: "${pageContext.request.contextPath}/admin/addParameterForm",

		    	    data: JSON.stringify(data.field),
//		    	    data: data.field,
		    	    contentType: "application/json; charset=utf-8",//(可以)

		    	    //dataType: "string",
		    	    success: function (result) {
		    	    	if(result=='success'){
		    				layer.msg('添加成功！');
		    			}else{
		    				layer.msg('添加失败！');
		    			}
		    	    }
		     });
			  return false;
		  });
		  });
	</script>    
    </body>
</html>
