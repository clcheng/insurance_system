<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>添加操作员</title>
</head>
    <body>
        <%@ include file="../header.jsp" %>
        <!--主要区域开始-->
        <div id="main" style="margin: auto;">
        	<jsp:include page="../system_maintenance.jsp"/>
        	<!-- 加载部分 -->
			<div style="font-size:14px;margin-top:15px;margin-bottom:15px">操作员列表</div>
			<table class="layui-hide" id="demo" lay-filter="table"></table>
        </div>
        <!--主要区域结束-->
        <jsp:include page="../footer.jsp"/>
    <script>
layui.use('table', function(){
  var table = layui.table;
  
  //展示已知数据
  table.render({
    elem: '#demo'
    ,url:'${pageContext.request.contextPath}/admin/list'
    ,cols: [[ //标题栏
      {field: 'userid', title: '工作证号', width: 120, sort: true}
      ,{field: 'username', title: '用户名', width: 240}
      ,{field: '', title: '操作', width: 360, toolbar: '#barDemo'}
    ]]
    //,skin: 'line' //表格风格
    ,even: true
    ,page: true //是否显示分页
    ,limits: [5, 7, 10]
    ,limit: 5 //每页默认显示的数量
  });
//监听工具条
  table.on('tool(table)', function(obj){
    var data = obj.data;
    if(obj.event === 'del'){
      layer.confirm('真的删除行么', function(index){
        $.ajax({
        	url:'${pageContext.request.contextPath}/admin/delete',
        	type: "POST",
            data:{"userid":data.userid},
            dataType: "json",
            success: function(data){
                if(data=="success"){
                   //删除这一行
                    obj.del();
                   //关闭弹框
                    layer.close(index);
                    layer.msg("删除成功", {icon: 6});
                }else{
                    layer.msg("删除失败", {icon: 5});
                }
            }
        });
      });
    } else if(obj.event === 'edit'){
    	window.location.href = "${pageContext.request.contextPath}/admin/editAdmin?adminId="+data.userid;
    }
  });
});
</script>
<script type="text/html" id="barDemo">
  <a class="layui-btn layui-btn-xs" lay-event="edit">修改</a>
  <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</script>
    </body>
</html>
