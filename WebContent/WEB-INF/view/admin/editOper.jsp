<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<title>添加操作员</title>
</head>
    <body>
        <%@ include file="../header.jsp" %>
        <!--主要区域开始-->
        <div id="main" style="margin: auto;">
        	<jsp:include page="../system_maintenance.jsp"/>
        	<!-- 加载部分 -->
			<div style="font-size:14px;margin-top:15px;margin-bottom:15px">添加操作员</div>
			<form class="layui-form layui-form-pane" action="${pageContext.request.contextPath}/admin/addForm" method="post" style="margin:auto;">
				<div class="layui-form-item">
			    	<label class="layui-form-label">工作证号</label>
				    <div class="layui-input-inline">
				      <input type="text" name="userid" lay-verify="required" value="${admin.userid }" autocomplete="off" class="layui-input" readonly="readonly">
				    </div>
				  </div>
			      <div class="layui-form-item">
				    <label class="layui-form-label">姓名</label>
				    <div class="layui-input-inline">
				      <input name="username" lay-verify="required" autocomplete="off" value="${admin.username }" class="layui-input" type="text">
				    </div>
				  </div>
			      <div class="layui-form-item">
				    <label class="layui-form-label">密码</label>
				    <div class="layui-input-inline">
				      <input type="password" name="password" id="password" autocomplete="off" class="layui-input">
				    </div>
				  </div>
			      <div class="layui-form-item">
				    <label class="layui-form-label">确认密码</label>
				    <div class="layui-input-inline">
				      <input type="password" id="rePassword" autocomplete="off" class="layui-input">
				    </div>
				  </div>
			    <div class="layui-form-item">
				    <div class="layui-input-block">
				      <button class="layui-btn" lay-submit="" lay-filter="add">立即提交</button>
				      <button type="reset" class="layui-btn layui-btn-primary">重置</button>
				    </div>
		  		</div>
			</form>
        </div>
        <!--主要区域结束-->
        <jsp:include page="../footer.jsp"/>
    <script>
    layui.use('form', function(){
  	  var form = layui.form;
		  //监听提交
		  form.on('submit(add)', function(data){
			  var pass = $("#password").val();
			  var rePass = $("#rePassword").val();
			  if(pass != rePass){
				  layer.msg('密码不一致！');
				  return false;
			  }else
			  $.ajax({
		    	    type: "post",
		    	    url: "${pageContext.request.contextPath}/admin/editForm",

		    	    data: JSON.stringify(data.field),
//		    	    data: data.field,
		    	    contentType: "application/json; charset=utf-8",//(可以)

		    	    //dataType: "string",
		    	    success: function (result) {
		    	    	if(result=='success'){
		    				layer.msg('添加成功！');
		    			}else if(result=='fail1'){
		    				layer.msg('编号已存在！');
		    			}else if(result=='fail2'){
		    				layer.msg('不存在该单位！');
		    			}else{
		    				layer.msg('添加失败！');
		    			}
		    	    }
		     });
			  return false;
		  });
		  });
	</script>    
    </body>
</html>
