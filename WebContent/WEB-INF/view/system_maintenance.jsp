﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<ul class="layui-nav layui-bg-blue" id="fa-opera">
  <li class="layui-nav-item">
    <a href="javascript:;">操作员管理</a>
    <dl class="layui-nav-child">
      <dd><a href="${pageContext.request.contextPath}/admin/add">录入操作员</a></dd>
      <dd><a href="${pageContext.request.contextPath}/admin/edit">进行操作</a></dd>
	<!-- <dd class="layui-this"><a href="">修改单位</a></dd> -->
    </dl>
  </li>
  <li class="layui-nav-item">
    <a href="javascript:;">系统参数</a>
    <dl class="layui-nav-child">
      <dd><a href="${pageContext.request.contextPath}/admin/addParameter">录入系统参数</a></dd>
      <dd><a href="${pageContext.request.contextPath}/admin/updateParameter">修改系统参数</a></dd>
  </li>
</ul>