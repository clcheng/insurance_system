﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<ul class="layui-nav layui-bg-blue" id="fa-opera">
  <li class="layui-nav-item">
    <a href="javascript:;">发放计算</a>
    <dl class="layui-nav-child">
      <dd><a href="${pageContext.request.contextPath}/grant/monthCount">按月发放计算</a></dd>
      <dd><a href="${pageContext.request.contextPath}/grant/oneCount">一次性发放计算</a></dd>
      <dd><a href="${pageContext.request.contextPath}/grant/deadCount">后事费用计算</a></dd>
    </dl>
  </li>
  <li class="layui-nav-item">
    <a href="javascript:;">发放记账</a>
    <dl class="layui-nav-child">
      <dd><a href="${pageContext.request.contextPath}/grant/monthFind">按月发放记账</a></dd>
      <dd><a href="${pageContext.request.contextPath}/grant/oneFind">一次性发放记账</a></dd>
      <dd><a href="${pageContext.request.contextPath}/grant/deadFind">后事费用记账</a></dd>
  </li>
</ul>