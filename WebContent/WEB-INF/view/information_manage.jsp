﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<ul class="layui-nav layui-bg-blue" id="fa-opera">
  <li class="layui-nav-item">
    <a href="javascript:;">档案查询</a>
    <dl class="layui-nav-child">
      	<dd><a href="${pageContext.request.contextPath}/findInfo/company">单位档案</a></dd>
	    <dd><a href="${pageContext.request.contextPath}/findInfo/work">在职职工档案</a></dd>
      	<dd><a href="${pageContext.request.contextPath}/findInfo/retired">退休职工档案</a></dd>
      	<dd><a href="${pageContext.request.contextPath}/findInfo/dead">死亡职工档案</a></dd>
	<!-- <dd class="layui-this"><a href="">修改单位</a></dd> -->
    </dl>
  </li>
  <li class="layui-nav-item">
    <a href="javascript:;">缴费查询</a>
    <dl class="layui-nav-child">
      <dd><a href="${pageContext.request.contextPath}/findInfo/payTally">单位缴费</a></dd>
      <dd><a href="${pageContext.request.contextPath}/findInfo/workpay">职工缴费</a></dd>
    </dl>
  </li>
  <li class="layui-nav-item">
    <a href="javascript:;">发放查询</a>
    <dl class="layui-nav-child">
      <dd><a href="${pageContext.request.contextPath}/findInfo/monthFind">保险金按月发放查询</a></dd>
      <dd><a href="${pageContext.request.contextPath}/findInfo/oneFind">保险金一次性发放查询</a></dd>
      <dd><a href="${pageContext.request.contextPath}/findInfo/deadFind">死亡清算查询</a></dd>
    </dl>
  </li>
</ul>