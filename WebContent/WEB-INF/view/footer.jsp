﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
</head>
   <div id="footer">
       <p>[最真实的企业环境，最适用的实战项目]</p>
       <p>版权所有(C)XXX集团公司 </p>
   </div>
<script>
    layui.use(['form', 'layedit', 'laydate'], function(){
		  var form = layui.form
		  ,layer = layui.layer
		  ,layedit = layui.layedit
		  ,laydate = layui.laydate;
		  
		  //日期
		  laydate.render({
		    elem: '#date'
		  });
		  laydate.render({
		    elem: '#date1'
		  });
		  laydate.render({
		    elem: '#date2'
		  });
		  
		  layui.use('element', function(){
			  var element = layui.element; //导航的hover效果、二级菜单等功能，需要依赖element模块
			  
			  //监听导航点击
			  element.on('nav(demo)', function(elem){
			    //console.log(elem)
			    layer.msg(elem.text());
			  });
			});
		  //创建一个编辑器
		  //var editIndex = layedit.build('LAY_demo_editor');
		 
		  //自定义验证规则
		  form.verify({
		    title: function(value){
		      if(value.length < 5){
		        return '标题至少得5个字符啊';
		      }
		    }
		    ,pass: [/(.+){6,12}$/, '密码必须6到12位']
		    ,content: function(value){
		      layedit.sync(editIndex);
		    }
		  });
    })
</script>
    </body>
</html>
