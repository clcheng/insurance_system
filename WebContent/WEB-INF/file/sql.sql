/*
SQLyog Ultimate v12.08 (32 bit)
MySQL - 5.5.36 : Database - assurance
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`assurance` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `assurance`;

/*Table structure for table `admin` */

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin` (
  `userid` varchar(25) NOT NULL COMMENT '工作证号',
  `username` varchar(25) NOT NULL COMMENT '用户真实姓名',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `pri` int(2) NOT NULL COMMENT '权限',
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `admin` */

insert  into `admin`(`userid`,`username`,`password`,`pri`) values ('AA0001','cl','4QrcOUm6Wau+VuBX8g+IPg==',0),('AA0002','Xiaochao','4QrcOUm6Wau+VuBX8g+IPg==',0),('AA0010','zyy','4QrcOUm6Wau+VuBX8g+IPg==',0),('AD0001','root','4QrcOUm6Wau+VuBX8g+IPg==',1);

/*Table structure for table `compaccount` */

DROP TABLE IF EXISTS `compaccount`;

CREATE TABLE `compaccount` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `comp_id` varchar(25) NOT NULL COMMENT '单位代号',
  `compacc_date` datetime NOT NULL COMMENT '缴费日期',
  `workacc_totalmoney` float(8,3) NOT NULL COMMENT '个人缴费总额',
  `compacc_totalmoney` float(8,3) NOT NULL COMMENT '单位缴费总额',
  `compacc_latermoney` float(8,3) NOT NULL COMMENT '单位补缴费用',
  `compacc_flag` int(2) NOT NULL COMMENT '缴费标记',
  PRIMARY KEY (`id`),
  KEY `compaccount_comp_id` (`comp_id`),
  CONSTRAINT `compaccount_comp_id` FOREIGN KEY (`comp_id`) REFERENCES `company` (`comp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;

/*Data for the table `compaccount` */

insert  into `compaccount`(`id`,`comp_id`,`compacc_date`,`workacc_totalmoney`,`compacc_totalmoney`,`compacc_latermoney`,`compacc_flag`) values (47,'AA0000','2011-04-02 13:31:59',1164.920,3349.146,0.000,1),(48,'AA0000','2011-06-03 13:39:32',1164.920,3349.146,0.000,1);

/*Table structure for table `company` */

DROP TABLE IF EXISTS `company`;

CREATE TABLE `company` (
  `comp_id` varchar(25) NOT NULL COMMENT '单位代号',
  `comp_name` varchar(50) NOT NULL COMMENT '单位名称',
  `comp_phone` varchar(25) NOT NULL COMMENT '单位电话',
  `comp_address` varchar(50) NOT NULL COMMENT '单位地址',
  `comp_post` varchar(25) DEFAULT NULL COMMENT '单位邮编',
  `comp_law` varchar(25) NOT NULL COMMENT '法定代表',
  `comp_lawid` varchar(25) NOT NULL COMMENT '法定代表证件号',
  `comp_type` varchar(25) NOT NULL COMMENT '单位类型',
  `comp_piece` varchar(25) NOT NULL COMMENT '单位所在区县',
  `comp_accid` varchar(25) NOT NULL COMMENT '单位账户号',
  `comp_leftaccount` float(8,3) NOT NULL,
  `com_date` datetime NOT NULL COMMENT '单位参保日期',
  `com_ratio` float(8,6) NOT NULL COMMENT '缴费比率',
  `comp_state` int(8) NOT NULL COMMENT '单位状态',
  PRIMARY KEY (`comp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `company` */

insert  into `company`(`comp_id`,`comp_name`,`comp_phone`,`comp_address`,`comp_post`,`comp_law`,`comp_lawid`,`comp_type`,`comp_piece`,`comp_accid`,`comp_leftaccount`,`com_date`,`com_ratio`,`comp_state`) values ('AA0000','海辉软件','0510-22222222','江苏无锡','123456','孙振耀','454365','国有企业','无锡新区','1234567890123456',17279.227,'2002-06-01 00:00:00',0.230000,0),('AA1234','花旗银行','021-98032435','上海上海','234567','里斯','8790865','外资企业','上海','4567890987652345',0.000,'2011-03-10 00:00:00',0.456000,0),('AA2345','海尔集团','0123-3456734','中国山东青岛','214000','张瑞敏','1234567','私营企业','山东青岛','1234567812345679',0.000,'2011-03-20 00:00:00',0.237000,0),('AB1233','中软国际','2345-23456789','中国无锡','214000','张三','345789','国有企业','中国无锡新区','1234908712349087',0.000,'2011-03-09 00:00:00',0.345000,0),('AB1235','microsoft','0123-3456789','美国','123456','比尔盖茨','1234567','国有控股企业','美国','1234567812345679',0.000,'2011-03-22 00:00:00',0.236000,0),('AB4321','联想','1234-89012345','中国北京','345679','柳传志','7859988','国有企业','北京','6789032678967892',0.000,'2011-03-01 00:00:00',0.520000,0),('AB5678','腾讯QQ','4567-23455667','中国深圳','456789','马化腾','679890','国有企业','中国深证','2345678909876543',0.000,'2011-03-04 00:00:00',0.236000,0),('AK0008','安徽科技学院','0510-6756789','安徽凤阳','789098','张三','78900987654','事业单位','安徽凤阳','1234567890123456',0.000,'1998-06-01 00:00:00',0.201000,0);

/*Table structure for table `dead` */

DROP TABLE IF EXISTS `dead`;

CREATE TABLE `dead` (
  `dead_id` varchar(25) NOT NULL COMMENT '死亡人身份证号',
  `dead_name` varchar(25) NOT NULL COMMENT '死亡人姓名',
  `comp_id` varchar(25) NOT NULL COMMENT '所在单位代号',
  `dead_sex` varchar(2) NOT NULL COMMENT '死亡人姓名',
  `dead_accid` varchar(25) NOT NULL COMMENT '死亡人账户号',
  `dead_account` float(8,2) NOT NULL COMMENT '死亡人账户余额',
  `dead_relaid` varchar(25) NOT NULL COMMENT '待遇领取人身份证号',
  `dead_relaname` varchar(25) NOT NULL COMMENT '待遇领取人姓名',
  `dead_relasex` varchar(2) NOT NULL COMMENT '待遇领取人性别',
  `dead_relaphone` varchar(25) NOT NULL COMMENT '待遇领取人电话',
  `dead_relaaddress` varchar(50) NOT NULL COMMENT '待遇领取人地址',
  `dead_relapost` varchar(25) DEFAULT NULL COMMENT '待遇领取人邮编',
  `dead_help1` float(8,3) NOT NULL COMMENT '丧葬补助费',
  `dead_help2` float(8,3) NOT NULL COMMENT '抚恤费用',
  `dead_flag` int(2) NOT NULL COMMENT '发放标记',
  PRIMARY KEY (`dead_id`),
  KEY `dead_comp_id` (`comp_id`),
  CONSTRAINT `dead_comp_id` FOREIGN KEY (`comp_id`) REFERENCES `company` (`comp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `dead` */

insert  into `dead`(`dead_id`,`dead_name`,`comp_id`,`dead_sex`,`dead_accid`,`dead_account`,`dead_relaid`,`dead_relaname`,`dead_relasex`,`dead_relaphone`,`dead_relaaddress`,`dead_relapost`,`dead_help1`,`dead_help2`,`dead_flag`) values ('098765432112345678','zhaoliu','AB1235','男','1234567812345684',0.00,'121231234567890356','王五','男','1234-87654321','安徽蚌埠蚌山区','234576',0.000,0.000,0),('234123456543234567','William','AA0000','男','7867898765456789',0.00,'765678909876543234','Lisinopril','男','0987-09876543','江苏无锡','876543',2222.220,3456.000,1);

/*Table structure for table `message` */

DROP TABLE IF EXISTS `message`;

CREATE TABLE `message` (
  `id` int(4) NOT NULL AUTO_INCREMENT COMMENT '信息代号',
  `title` varchar(25) NOT NULL COMMENT '信息标题',
  `content` varchar(500) NOT NULL COMMENT '信息内容',
  `time` datetime NOT NULL COMMENT '信息发布日期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

/*Data for the table `message` */

insert  into `message`(`id`,`title`,`content`,`time`) values (2,'Helloworld','123456','2011-03-13 19:01:19'),(4,'First Message','Let me have a try !!!!!','2011-03-13 20:23:52'),(5,'lajsdf','qwer','2011-03-17 21:35:09'),(7,'sdfsf','sfdsfsaf','2011-03-19 15:25:27'),(8,'保险信息',' 第一条 为切实保障参加城镇企业职工基本养老保险人员（以下简称参保人员）的合法权益，促进人力资源合理配置和有序流动，保证参保人员跨省、自治区、直辖市（以下简称跨省）流动并在城镇就业时基本养老保险关系的顺畅转移接续，制定本办法。 </p>  <p> 第二条 本办法适用于参加城镇企业职工基本养老保险的所有人员，包括','2011-05-28 17:32:02'),(9,'dsf','zxvcvccc','2011-04-07 15:52:07'),(10,'ASqwerr','看的设计公司可根据','2011-04-26 22:18:13'),(11,'系统调试最后一天','终于快结束了，我真的都快崩溃了！！！','2011-04-02 13:03:31'),(12,'测试消息1','这仅仅是一个测试哦。。。','2018-04-06 00:24:04'),(13,'测试消息1','这仅仅是一个测试哦。。。11111','2018-04-06 00:25:24');

/*Table structure for table `retiraccount` */

DROP TABLE IF EXISTS `retiraccount`;

CREATE TABLE `retiraccount` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `retir_id` varchar(25) NOT NULL COMMENT '退休职工身份证号',
  `retiracc_date` datetime NOT NULL COMMENT '发放日期',
  `comp_id` varchar(25) NOT NULL COMMENT '所在单位代号',
  `retiracc_money` float(8,3) NOT NULL COMMENT '本月发放养老金总额',
  `retiracc_flag` int(2) NOT NULL COMMENT '发放标记',
  PRIMARY KEY (`id`),
  KEY `retiraccount_comp_id` (`comp_id`),
  KEY `retir_id` (`retir_id`),
  CONSTRAINT `retiraccount_ibfk_6` FOREIGN KEY (`retir_id`) REFERENCES `retired` (`retired_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `retiraccount_ibfk_9` FOREIGN KEY (`comp_id`) REFERENCES `company` (`comp_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=175 DEFAULT CHARSET=utf8;

/*Data for the table `retiraccount` */

insert  into `retiraccount`(`id`,`retir_id`,`retiracc_date`,`comp_id`,`retiracc_money`,`retiracc_flag`) values (37,'098765432112345678','2011-03-24 17:26:42','AB1235',4978.000,1),(164,'123456789987654321','2011-04-02 13:34:57','AA0000',1050.784,1),(166,'656789098765434567','2011-04-02 13:34:57','AA0000',1235.867,1),(173,'123456789987654321','2011-06-03 13:45:15','AA0000',1050.784,0),(174,'656789098765434567','2011-06-03 13:45:16','AA0000',1235.867,0);

/*Table structure for table `retired` */

DROP TABLE IF EXISTS `retired`;

CREATE TABLE `retired` (
  `retired_id` varchar(25) NOT NULL COMMENT '退休职工身份证号',
  `retired_name` varchar(25) NOT NULL COMMENT '退休职工姓名',
  `comp_id` varchar(25) NOT NULL COMMENT '所在单位代号',
  `retired_telephone` varchar(25) DEFAULT NULL,
  `retired_address` varchar(50) NOT NULL COMMENT '退休职工地址',
  `retired_post` varchar(25) NOT NULL COMMENT '退休职工邮编',
  `retired_sex` varchar(2) NOT NULL COMMENT '退休职工性别',
  `retired_birth` datetime NOT NULL COMMENT '退休职工出生日期',
  `retired_nation` varchar(25) NOT NULL COMMENT '退休职工民族',
  `retired_worktype` varchar(25) NOT NULL COMMENT '退休职工用工形式',
  `retired_type` varchar(25) NOT NULL COMMENT '退休职工职务',
  `retired_beginwork` datetime NOT NULL COMMENT '退休职工开始工作时间',
  `retired_begintime` datetime NOT NULL COMMENT '退休职工开始参保时间',
  `retired_retirtime` datetime NOT NULL COMMENT '退休职工退休时间',
  `retired_worktime` int(8) NOT NULL COMMENT '退休职工缴费年数',
  `retired_accid` varchar(25) NOT NULL COMMENT '退休职工账户号',
  `retired_account` float(8,3) NOT NULL COMMENT '户账总额',
  `retired_leftaccount` float(8,3) NOT NULL COMMENT '退休职工个人账户剩余',
  `retired_state` int(2) NOT NULL COMMENT '退休状态',
  PRIMARY KEY (`retired_id`),
  KEY `retired_comp_id` (`comp_id`),
  CONSTRAINT `retired_comp_id` FOREIGN KEY (`comp_id`) REFERENCES `company` (`comp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `retired` */

insert  into `retired`(`retired_id`,`retired_name`,`comp_id`,`retired_telephone`,`retired_address`,`retired_post`,`retired_sex`,`retired_birth`,`retired_nation`,`retired_worktype`,`retired_type`,`retired_beginwork`,`retired_begintime`,`retired_retirtime`,`retired_worktime`,`retired_accid`,`retired_account`,`retired_leftaccount`,`retired_state`) values ('098765432112345678','zhaoliu','AB1235','1349-0987654','江苏杭州','234598','男','2011-03-08 00:00:00','满族','固定','临时工','2011-03-08 00:00:00','2011-03-17 00:00:00','2011-03-07 00:00:00',3,'8765436789098765',1000.768,0.000,0),('123456789987654321','钱七','AA0000','1234-12345678','北京中关村','123456','男','2011-05-17 00:00:00','汉族','固定','Java开发工程师','2011-05-11 00:00:00','2011-05-05 00:00:00','2011-05-07 00:00:00',12,'1234567890123456',66358.062,54023.059,0),('656789098765434567','Sam','AA0000','020-98765432','广东广州','876567','男','1974-04-25 00:00:00','汉族','固定','花旗经理','1998-04-02 00:00:00','1999-03-27 00:00:00','2011-03-27 00:00:00',23,'1234567890987654',88567.977,84877.625,0);

/*Table structure for table `total` */

DROP TABLE IF EXISTS `total`;

CREATE TABLE `total` (
  `year` varchar(25) NOT NULL DEFAULT '' COMMENT '使用年度',
  `averagesalary` float(8,3) NOT NULL COMMENT '上一年度月平均工资',
  `totalmoney` float(8,3) NOT NULL COMMENT '社会统筹基金',
  `work_ratio` float(8,3) NOT NULL COMMENT '个人缴费比率',
  `workacc_ratio` float(8,3) NOT NULL COMMENT '划入个人账户比率',
  `comp_lateratio` float(8,3) NOT NULL COMMENT '单位补缴滞纳金比率',
  PRIMARY KEY (`year`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `total` */

insert  into `total`(`year`,`averagesalary`,`totalmoney`,`work_ratio`,`workacc_ratio`,`comp_lateratio`) values ('2011',2489.000,2257.000,0.080,0.800,0.002);

/*Table structure for table `workaccount` */

DROP TABLE IF EXISTS `workaccount`;

CREATE TABLE `workaccount` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `work_id` varchar(25) NOT NULL COMMENT '在职职工身份证号',
  `workacc_date` datetime NOT NULL COMMENT '缴费日期',
  `comp_id` varchar(25) NOT NULL COMMENT '所在单位代号',
  `work_salary` float(8,3) NOT NULL COMMENT '本月工资',
  `workacc_salary` float(8,3) NOT NULL COMMENT '本月缴费工资基数',
  `workacc_money` float(8,3) NOT NULL COMMENT '职工缴费',
  `compacc_money` float(8,3) NOT NULL COMMENT '公司缴费',
  `workacc_flag` int(2) NOT NULL COMMENT '缴费标志',
  PRIMARY KEY (`id`),
  KEY `workaccount_comp_id` (`comp_id`),
  KEY `work_id` (`work_id`),
  CONSTRAINT `workaccount_ibfk_1` FOREIGN KEY (`comp_id`) REFERENCES `company` (`comp_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `workaccount_ibfk_2` FOREIGN KEY (`work_id`) REFERENCES `worker` (`work_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8;

/*Data for the table `workaccount` */

insert  into `workaccount`(`id`,`work_id`,`workacc_date`,`comp_id`,`work_salary`,`workacc_salary`,`workacc_money`,`compacc_money`,`workacc_flag`) values (66,'345678890765432128','2011-04-02 13:31:59','AA0000',2000.123,2000.123,160.010,460.028,1),(67,'657890123456789012','2011-04-02 13:31:59','AA0000',3600.980,3600.980,288.078,828.225,1),(68,'765434567876543456','2011-04-02 13:31:59','AA0000',8000.990,7467.000,597.360,1717.410,1),(69,'876789098766543234','2011-04-02 13:31:59','AA0000',800.990,1493.400,119.472,343.482,1),(70,'345678890765432128','2011-06-03 13:39:32','AA0000',2000.123,2000.123,160.010,460.028,1),(71,'657890123456789012','2011-06-03 13:39:32','AA0000',3600.980,3600.980,288.078,828.225,1),(72,'765434567876543456','2011-06-03 13:39:32','AA0000',8000.990,7467.000,597.360,1717.410,1),(73,'876789098766543234','2011-06-03 13:39:32','AA0000',800.990,1493.400,119.472,343.482,1);

/*Table structure for table `worker` */

DROP TABLE IF EXISTS `worker`;

CREATE TABLE `worker` (
  `work_id` varchar(25) NOT NULL DEFAULT '' COMMENT '在职职工身份证号',
  `work_name` varchar(25) NOT NULL COMMENT '在职职工姓名',
  `comp_id` varchar(25) NOT NULL COMMENT '所在单位代号',
  `work_phone` varchar(25) NOT NULL COMMENT '在职职工电话',
  `work_address` varchar(50) NOT NULL COMMENT '在职职工地址',
  `work_post` varchar(25) NOT NULL COMMENT '在职职工邮编',
  `work_sex` varchar(2) NOT NULL COMMENT '在职职工性别',
  `work_birth` datetime NOT NULL COMMENT '在职职工出生日期',
  `work_nation` varchar(8) NOT NULL COMMENT '在职职工民族',
  `work_worktype` varchar(25) NOT NULL COMMENT '在职职工用工形式',
  `work_type` varchar(25) NOT NULL COMMENT '在职职工职务',
  `work_beginwork` datetime NOT NULL COMMENT '在职职工参加工作时间',
  `work_begintime` datetime NOT NULL COMMENT '在职职工参保时间',
  `work_accid` varchar(25) NOT NULL COMMENT '在职职工账户号',
  `work_account` float(8,3) NOT NULL COMMENT '在职职工个人账户余额',
  `work_state` int(2) NOT NULL COMMENT '在职状态',
  PRIMARY KEY (`work_id`),
  KEY `worker_comp_id` (`comp_id`),
  CONSTRAINT `worker_comp_id` FOREIGN KEY (`comp_id`) REFERENCES `company` (`comp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `worker` */

insert  into `worker`(`work_id`,`work_name`,`comp_id`,`work_phone`,`work_address`,`work_post`,`work_sex`,`work_birth`,`work_nation`,`work_worktype`,`work_type`,`work_beginwork`,`work_begintime`,`work_accid`,`work_account`,`work_state`) values ('345678890765432128','张三','AA0000','0510-2345678','安徽凤阳','123456','男','2011-05-01 00:00:00','朝鲜族','固定','学生','2011-05-09 00:00:00','2011-05-18 00:00:00','0987654321234567',1111.416,0),('657890123456789012','Jake','AA0000','028-09867678','重庆','765432','男','1985-04-19 00:00:00','其它','固定','英语培训师','1999-04-24 00:00:00','2003-03-30 00:00:00','8769098765434567',1786.086,0),('678901234567890123','赵六','AK0008','0520-12345678','安徽凤阳安徽科技学院','234000','男','1988-04-01 00:00:00','汉族','固定','学生','2007-06-01 00:00:00','2008-06-01 00:00:00','9876543212345678',0.000,0),('765434567876543456','Alvin','AA0000','0510-98765456','蠡湖大道99号','214000','男','1988-04-02 00:00:00','汉族','固定','java开发程序员','2010-04-02 00:00:00','2010-04-09 00:00:00','5676543234567876',3703.632,0),('876789098766543234','Lisa','AA0000','7654-9876543','山西太原','890876','女','1978-04-02 00:00:00','汉族','固定','人事助理','1999-03-30 00:00:00','2002-04-02 00:00:00','7867565432456789',740.726,0);

/*Table structure for table `comstuff` */

DROP TABLE IF EXISTS `comstuff`;

/*!50001 DROP VIEW IF EXISTS `comstuff` */;
/*!50001 DROP TABLE IF EXISTS `comstuff` */;

/*!50001 CREATE TABLE  `comstuff`(
 `comp_id` varchar(25) ,
 `comp_name` varchar(50) ,
 `retired_id` varchar(25) ,
 `dead_id` varchar(25) ,
 `work_name` varchar(25) ,
 `retired_name` varchar(25) ,
 `dead_name` varchar(25) 
)*/;

/*View structure for view comstuff */

/*!50001 DROP TABLE IF EXISTS `comstuff` */;
/*!50001 DROP VIEW IF EXISTS `comstuff` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `comstuff` AS select `company`.`comp_id` AS `comp_id`,`company`.`comp_name` AS `comp_name`,`retired`.`retired_id` AS `retired_id`,`dead`.`dead_id` AS `dead_id`,`worker`.`work_name` AS `work_name`,`retired`.`retired_name` AS `retired_name`,`dead`.`dead_name` AS `dead_name` from (((`company` join `dead`) join `retired`) join `worker`) where ((`worker`.`comp_id` = `company`.`comp_id`) or (`dead`.`comp_id` = `company`.`comp_id`) or (`retired`.`comp_id` = `company`.`comp_id`)) */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
