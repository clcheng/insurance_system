package com.zyy.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.zyy.dao.DeadDao;
import com.zyy.entity.Dead;

@Service
public class DeadServiceImpl implements DeadService{ 
	@Resource
	DeadDao deadDao;

	@Override
	public Integer addDead(Dead dead) {
		return deadDao.addDead(dead);
	}

	@Override
	public Integer findDead(Dead dead) {
		return deadDao.findDead(dead);
	}

	@Override
	public Integer editDead(Dead dead) {
		return  deadDao.editDead(dead);
	}

	@Override
	public Dead findDeadName(String workId) {
		return deadDao.findDeadName(workId);
	}

}
