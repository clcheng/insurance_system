package com.zyy.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.zyy.dao.AdminDao;
import com.zyy.entity.Admin;

@Service
public class AdminServiceImpl implements AdminService{ 
	@Resource
    AdminDao admitDao;
	@Override
	public Admin findAdmin(Admin admin) {
		return admitDao.findAdmin(admin);
	}
	@Override
	public Integer addAdmin(Admin admin) {
		return admitDao.addAdmin(admin);
	}
	@Override
	public List<Admin> list() {
		return admitDao.list();
	}
	@Override
	public Integer count() {
		return admitDao.count();
	}
	@Override
	public Integer delete(String userId) {
		Integer aaa = admitDao.delete(userId);
		return admitDao.delete(userId);
	}
	@Override
	public Admin findName(String adminId) {
		return admitDao.findName(adminId);
	}
	@Override
	public Integer editAdmin(Admin admin) {
		return admitDao.editAdmin(admin);
	}


}
