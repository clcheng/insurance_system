package com.zyy.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.zyy.dao.AdminDao;
import com.zyy.dao.WorkerDao;
import com.zyy.entity.Admin;
import com.zyy.entity.Worker;

@Service
public class WorkerServiceImpl implements WorkerService{ 
	@Resource
	WorkerDao workerDao;

	@Override
	public Integer addWorker(Worker worker) {
		return workerDao.addWorker(worker);
	}

	@Override
	public Integer findWorker(Worker worker) {
		return workerDao.findWorker(worker);
	}

	@Override
	public Integer editWorker(Worker worker) {
		return  workerDao.editWorker(worker);
	}

	@Override
	public Worker findWorkerName(String workId) {
		return workerDao.findWorkerName(workId);
	}
	
	@Override
	public Worker findWorkerNa(String work_name) {
		return workerDao.findWorkerNa(work_name);
	}

	@Override
	public Integer delWorker(String retiredId) {
		// TODO zyy-generated method stub
		return workerDao.delWorker(retiredId);
	}

	@Override
	public Integer findCompId(String comp_id) {
		// TODO 鑷姩鐢熸垚鐨勬柟娉曞瓨鏍�
		return workerDao.findCompId(comp_id);
	}

	@Override
	public Worker findWorkAndComp(String worker, String company) {
		return workerDao.findWorkAndComp(worker,company);
	}

	@Override
	public void updateAccount(String work_id, float wcMoney) {
		workerDao.updateAccount(work_id,wcMoney);
	}

}
