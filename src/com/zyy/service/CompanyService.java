package com.zyy.service;

import com.zyy.entity.Company;

public interface CompanyService {
	 public Integer addCompany(Company company);

	public Integer findCompany(Company company);

	public Integer editCompany(Company company);

	public Company findName(String compId);
	
}
