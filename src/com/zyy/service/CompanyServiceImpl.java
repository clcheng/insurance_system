package com.zyy.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zyy.dao.AdminDao;
import com.zyy.dao.CompanyDao;
import com.zyy.dao.PayDao;
import com.zyy.entity.Admin;
import com.zyy.entity.Company;

@Service
public class CompanyServiceImpl implements CompanyService{ 
	@Resource
	private CompanyDao companyDao;
	@Resource
	private PayDao payDao;

	@Override
	public Integer addCompany(Company company) {
		payDao.addCompany(company.getComp_id());
		return companyDao.addCompany(company);
	}

	@Override
	public Integer findCompany(Company company) {
		return companyDao.findCompany(company);
	}

	@Override
	public Integer editCompany(Company company) {
		return  companyDao.editCompany(company);
	}

	@Override
	public Company findName(String compId) {
		return companyDao.findName(compId);
	}

}
