package com.zyy.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.zyy.dao.GrantDao;
import com.zyy.entity.Retiraccount;

@Service
public class GrantServiceImpl implements GrantService{ 
	@Resource
    GrantDao grantDao;

	@Override
	public Integer findTenForm(String retired_id) {
		return grantDao.findTenForm(retired_id);
	}

	@Override
	public void insert(Retiraccount retiraccount) {
		grantDao.insert(retiraccount);
	}

	@Override
	public Integer findForm(String retired_id) {
		return grantDao.findForm(retired_id);
	}

	@Override
	public List<Retiraccount> listRetiraccount(Retiraccount retiraccount) {
		return grantDao.listRetiraccount(retiraccount);
	}

	@Override
	public int countRetiraccount(Retiraccount retiraccount) {
		return grantDao.countRetiraccount(retiraccount);
	}

	@Override
	public Integer findDeadForm(String dead_id) {
		return grantDao.findDeadForm(dead_id);
	}

}
