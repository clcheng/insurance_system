package com.zyy.service;

import com.zyy.entity.Dead;

public interface DeadService {
	 public Integer addDead(Dead dead);

	public Integer findDead(Dead dead);

	public Integer editDead(Dead dead);

	public Dead findDeadName(String workId);
	
}
