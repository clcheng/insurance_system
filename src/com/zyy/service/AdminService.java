package com.zyy.service;

import java.util.List;

import com.zyy.entity.Admin;

public interface AdminService {
	 public Admin findAdmin(Admin admin);

	public Integer addAdmin(Admin admin);

	public List<Admin> list();

	public Integer count();

	public Integer delete(String userId);

	public Admin findName(String adminId);

	public Integer editAdmin(Admin admin);
	
}
