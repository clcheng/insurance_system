package com.zyy.service;

import java.util.List;

import com.zyy.entity.Compaccount;
import com.zyy.entity.CompaccountDown;
import com.zyy.entity.Workaccount;

public interface PayService {

	public Integer addWage(Workaccount workaccount);

	public Integer findCompany(Compaccount compaccount);

	public CompaccountDown countMoney(String compId);

	public void insertCompAccount(CompaccountDown down);

	public List<Compaccount> findCompaccount(String compId);

	public Integer findCompaccountByDate(String compId, String firstDay, String lastDay);

	public int countCompaccount(Compaccount compaccount);

	public List<Compaccount> listCompaccount(Compaccount compaccount);

	public List<Workaccount> listWorkaccount(Workaccount workaccount);

	public int countWorkaccount(Workaccount workaccount);

	
}
