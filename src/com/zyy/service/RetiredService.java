package com.zyy.service;

import com.zyy.entity.Dead;
import com.zyy.entity.Retiraccount;
import com.zyy.entity.Retired;

public interface RetiredService {
	 public Integer addRetired(Retired retired);

	public Integer findRetired(Retired retired);

	public Integer editRetired(Retired retired);

	public Retired findRetiredName(String workId);

	public Dead findDeadName(String retired_id);

}
