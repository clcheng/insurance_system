package com.zyy.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.zyy.dao.RetiredDao;
import com.zyy.entity.Dead;
import com.zyy.entity.Retiraccount;
import com.zyy.entity.Retired;

@Service
public class RetiredServiceImpl implements RetiredService{ 
	@Resource
	RetiredDao retiredDao;

	@Override
	public Integer addRetired(Retired retired) {
		return retiredDao.addRetired(retired);
	}

	@Override
	public Integer findRetired(Retired retired) {
		return retiredDao.findRetired(retired);
	}

	@Override
	public Integer editRetired(Retired retired) {
		return  retiredDao.editRetired(retired);
	}

	@Override
	public Retired findRetiredName(String workId) {
		return retiredDao.findRetiredName(workId);
	}

	@Override
	public Dead findDeadName(String dead_id) {
		return retiredDao.findDeadName(dead_id);
	}

}
