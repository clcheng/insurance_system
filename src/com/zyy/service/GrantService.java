package com.zyy.service;

import java.util.List;

import com.zyy.entity.Retiraccount;

public interface GrantService {

	Integer findTenForm(String retired_id);

	void insert(Retiraccount retiraccount);

	Integer findForm(String retired_id);

	List<Retiraccount> listRetiraccount(Retiraccount retiraccount);

	int countRetiraccount(Retiraccount retiraccount);

	Integer findDeadForm(String dead_id);

}
