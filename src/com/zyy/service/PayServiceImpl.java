package com.zyy.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.zyy.dao.AdminDao;
import com.zyy.dao.PayDao;
import com.zyy.entity.Admin;
import com.zyy.entity.Compaccount;
import com.zyy.entity.CompaccountDown;
import com.zyy.entity.Workaccount;

@Service
public class PayServiceImpl implements PayService{ 
	@Resource
    PayDao payDao;

	@Override
	public Integer addWage(Workaccount workaccount) {
		return payDao.addWage(workaccount);
	}

	@Override
	public Integer findCompany(Compaccount compaccount) {
		return payDao.findCompany(compaccount);
	}

	@Override
	public CompaccountDown countMoney(String compId) {
		return payDao.countMoney(compId);
	}

	@Override
	public void insertCompAccount(CompaccountDown down) {
		payDao.insertCompAccount(down);
	}

	@Override
	public List<Compaccount> findCompaccount(String compId) {
		return payDao.findCompaccount(compId);
	}

	@Override
	public Integer findCompaccountByDate(String compId, String firstDay, String lastDay) {
		return payDao.findCompaccountByDate(compId,firstDay,lastDay);
	}

	@Override
	public int countCompaccount(Compaccount compaccount) {
		return payDao.countCompaccount(compaccount);
	}

	@Override
	public List<Compaccount> listCompaccount(Compaccount compaccount) {
		return payDao.listCompaccount(compaccount);
	}

	@Override
	public List<Workaccount> listWorkaccount(Workaccount workaccount) {
		return payDao.listWorkaccount(workaccount);
	}

	@Override
	public int countWorkaccount(Workaccount workaccount) {
		return payDao.countWorkaccount(workaccount);
	}

}
