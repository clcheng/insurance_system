package com.zyy.service;

import com.zyy.entity.Total;

public interface TotalService {

	Total findParam();

	Integer addTotal(Total total);

	Integer updateTotal(Total total);
	
}
