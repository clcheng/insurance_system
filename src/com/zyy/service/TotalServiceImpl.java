package com.zyy.service;


import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.zyy.dao.TotalDao;
import com.zyy.entity.Total;

@Service
public class TotalServiceImpl implements TotalService{ 
	@Resource
    TotalDao totalDao;

	@Override
	public Total findParam() {
		return totalDao.findParam();
	}

	@Override
	public Integer addTotal(Total total) {
		return totalDao.addTotal(total);
	}

	@Override
	public Integer updateTotal(Total total) {
		return totalDao.updateTotal(total);
	}

}
