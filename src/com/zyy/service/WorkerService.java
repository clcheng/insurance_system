package com.zyy.service;

import com.zyy.entity.Worker;

public interface WorkerService {
	 public Integer addWorker(Worker worker);

	public Integer findWorker(Worker worker);

	public Integer editWorker(Worker worker);

	public Worker findWorkerName(String workId);
	
	public Worker findWorkerNa(String work_name);

	public Integer delWorker(String retiredId);

	public Integer findCompId(String comp_id);

	public Worker findWorkAndComp(String worker, String company);

	public void updateAccount(String work_id, float wcMoney);
	
}
