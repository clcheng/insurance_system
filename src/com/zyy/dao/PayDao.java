package com.zyy.dao;


import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.zyy.common.MyBatisRegister;
import com.zyy.entity.Compaccount;
import com.zyy.entity.CompaccountDown;
import com.zyy.entity.Workaccount;
@MyBatisRegister
public interface PayDao {

	public Integer addWage(Workaccount workaccount);

	public CompaccountDown countMoney(String compId);

	public CompaccountDown CompaccountDown(String compId);

	public Integer findCompany(Compaccount compaccount);

	public void insertCompAccount(CompaccountDown down);

	public List<Compaccount> findCompaccount(String compId);

	public Integer findCompaccountByDate(@Param("compId")String compId, @Param("firstDay")String firstDay, @Param("lastDay")String lastDay);

	public int countCompaccount(Compaccount compaccount);

	public List<Compaccount> listCompaccount(Compaccount compaccount);

	public List<Workaccount> listWorkaccount(Workaccount workaccount);

	public int countWorkaccount(Workaccount workaccount);

	public void addCompany(String comp_id);

}
