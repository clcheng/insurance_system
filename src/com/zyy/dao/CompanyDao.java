package com.zyy.dao;

import com.zyy.common.MyBatisRegister;
import com.zyy.entity.Company;
@MyBatisRegister
public interface CompanyDao {
	public Integer addCompany(Company company);

	public Integer findCompany(Company company);

	public Integer editCompany(Company company);

	public Company findName(String comp_id);
}
