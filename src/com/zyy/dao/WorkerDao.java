package com.zyy.dao;

import org.apache.ibatis.annotations.Param;

import com.zyy.common.MyBatisRegister;
import com.zyy.entity.Worker;
@MyBatisRegister
public interface WorkerDao {
	public Integer addWorker(Worker worker);

	public Integer findWorker(Worker worker);

	public Integer editWorker(Worker worker);

	public Worker findWorkerName(String work_id);

	public Integer delWorker(String retired_id);

	public Integer findCompId(String comp_id);

	public Worker findWorkAndComp(@Param("work_id")String work_id, @Param("company")String company);

	public void updateAccount(@Param("work_id")String work_id, @Param("wcMoney")float wcMoney);

	public Worker findWorkerNa(String work_name);
}
