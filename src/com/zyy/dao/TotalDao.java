package com.zyy.dao;

import com.zyy.common.MyBatisRegister;
import com.zyy.entity.Total;
@MyBatisRegister
public interface TotalDao {

	Total findParam();

	Integer addTotal(Total total);

	Integer updateTotal(Total total);
}
