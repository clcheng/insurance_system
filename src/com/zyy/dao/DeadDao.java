package com.zyy.dao;

import com.zyy.common.MyBatisRegister;
import com.zyy.entity.Dead;
@MyBatisRegister
public interface DeadDao {
	public Integer addDead(Dead dead);

	public Integer findDead(Dead dead);

	public Integer editDead(Dead dead);

	public Dead findDeadName(String work_id);
}
