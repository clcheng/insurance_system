package com.zyy.dao;

import com.zyy.common.MyBatisRegister;
import com.zyy.entity.Dead;
import com.zyy.entity.Retiraccount;
import com.zyy.entity.Retired;
@MyBatisRegister
public interface RetiredDao {
	public Integer addRetired(Retired retired);

	public Integer findRetired(Retired retired);

	public Integer editRetired(Retired retired);

	public Retired findRetiredName(String work_id);

	public Dead findDeadName(String dead_id);

}
