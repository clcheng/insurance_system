package com.zyy.dao;


import java.util.List;

import com.zyy.common.MyBatisRegister;
import com.zyy.entity.Retiraccount;
@MyBatisRegister
public interface GrantDao {

	public Integer findTenForm(String retired_id);

	public void insert(Retiraccount retiraccount);

	public Integer findForm(String retired_id);

	public List<Retiraccount> listRetiraccount(Retiraccount retiraccount);

	public int countRetiraccount(Retiraccount retiraccount);

	public Integer findDeadForm(String dead_id);

}
