package com.zyy.dao;

import java.util.List;

import com.zyy.common.MyBatisRegister;
import com.zyy.entity.Admin;
@MyBatisRegister
public interface AdminDao {
	public Admin findAdmin(Admin admin);

	public Integer addAdmin(Admin admin);

	public List<Admin> list();

	public Integer count();

	public Integer delete(String userId);

	public Admin findName(String adminId);

	public Integer editAdmin(Admin admin);
}
