package com.zyy.controller;


import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zyy.entity.Company;
import com.zyy.service.CompanyService;

@Controller
@RequestMapping("company")
public class CompanyController {
	
	@Resource
	private CompanyService companyService;
	
	@RequestMapping("/add")
	public String add(Model model){
		return "company/addCompany";
	}
	@RequestMapping("/find")
	public String find(Model model){
		return "company/findCompany";
	}
	@RequestMapping("/edit")
	public String edit(Model model,HttpServletRequest request){
		String compId = request.getParameter("compId");
		Company company = companyService.findName(compId);
		model.addAttribute("company", company);
		return "company/editCompany";
	}
	
	@RequestMapping(value={"/addForm"},method={RequestMethod.POST},consumes={"application/json", "application/xml"})
	@ResponseBody
	public String addForm(@RequestBody Company company){
		Integer result = 0;
		try {
			result = companyService.addCompany(company);
		} catch (Exception e) {
			return "fail1";
		}
		if(result>0){
			return "success";
		}else{
			return "fail";
		}
	}
		
		@RequestMapping("/editForm")
		@ResponseBody
		public String editForm(@RequestBody Company company){
			Integer result = 0;
			/*try {*/
				result = companyService.editCompany(company);
			/*} catch (Exception e) {
				return "fail1";
			}*/
			if(result>0){
				return "success";
			}else{
				return "fail";
			}
	}
	
	@RequestMapping("/findForm")
	@ResponseBody
	public String findForm(Company company,HttpServletRequest request){
//		HttpSession session = request.getSession();
//		session.setAttribute("compId", company.getCompId());
		Integer result = 0;
		try {
			result = companyService.findCompany(company);
		} catch (Exception e) {
			return "fail";
		}
		if(result>0){
			return "success";
		}else{
			return "fail1";
		}
	}
	
}

