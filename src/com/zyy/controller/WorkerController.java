package com.zyy.controller;


import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zyy.entity.Dead;
import com.zyy.entity.Retired;
import com.zyy.entity.Worker;
import com.zyy.service.DeadService;
import com.zyy.service.RetiredService;
import com.zyy.service.WorkerService;

@Controller
@RequestMapping("worker")
public class WorkerController {
	
	@Resource
	private WorkerService workerService;
	@Autowired
	private RetiredService retiredService;
	@Autowired
	private DeadService deadService;
	
	@RequestMapping("/add")
	public String add(Model model){
		return "worker/addWorker";
	}
	@RequestMapping("/find")
	public String find(Model model){
		return "worker/findWorker";
	}
	@RequestMapping("/edit")
	public String edit(Model model,HttpServletRequest request){
		String workId = request.getParameter("workId");
		Worker worker = workerService.findWorkerName(workId);
		model.addAttribute("worker", worker);
		return "worker/editWorker";
	}
	@RequestMapping("/findRetire")
	public String findRetire(Model model){
		return "worker/findRetire";
	}
	@RequestMapping("/findDead")
	public String findDead(Model model){
		return "worker/findDead";
	}
	@RequestMapping("/retire")
	public String retire(HttpServletRequest request,Model model){
		String workId = request.getParameter("workId");
		Worker worker = workerService.findWorkerName(workId);
		model.addAttribute("worker",worker);
		return "worker/editRetire";
	}
	@RequestMapping("/dead")
	public String dead(HttpServletRequest request,Model model){
		String workId = request.getParameter("workId");
		Worker worker = workerService.findWorkerName(workId);
		model.addAttribute("worker",worker);
		return "worker/editDead";
	}
	
	@RequestMapping(value={"/addForm"},method={RequestMethod.POST},consumes={"application/json", "application/xml"})
	@ResponseBody
	public String addForm(@RequestBody Worker worker){
		Integer result = 0;
		if(workerService.findWorkerName(worker.getWork_id())!=null){
			return "fail1";
		}else if(workerService.findCompId(worker.getComp_id())==0){
			return "fail2";
		}else{
			result = workerService.addWorker(worker);
			if(result>0){
				return "success";
			}else{
				return "fail";
			}
		}
	}
		
	@RequestMapping("/editForm")
	@ResponseBody
	public String editForm(@RequestBody Worker worker){
		Integer result = 0;
		if(workerService.findCompId(worker.getComp_id())==0){
			return "fail1";
		}else{
			result = workerService.editWorker(worker);
			if(result>0){
				return "success";
			}else{
				return "fail";
			}
		}
	}
	
	@RequestMapping("/addRetireForm")
	@ResponseBody
	public String addRetireForm(@RequestBody Retired retired){
		Integer result = 0;
		Retired workRetire = retiredService.findRetiredName(retired.getRetired_id());
		if(workRetire!=null){
			return "fail1";
		}else{
			result = retiredService.addRetired(retired);
			workerService.delWorker(retired.getRetired_id());
		}
		if(result>0){
			return "success";
		}else{
			return "fail";
		}
	}
	
	@RequestMapping("/addDeadForm")
	@ResponseBody
	public String addDeadForm(@RequestBody Dead dead){
		Integer result = 0;
		Dead workDead = deadService.findDeadName(dead.getDead_id());
		if(workDead!=null){
			return "fail1";
		}else{
			result = deadService.addDead(dead);
			workerService.delWorker(dead.getDead_id());
		}
		if(result>0){
			return "success";
		}else{
			return "fail";
		}
	}
	
	@RequestMapping("/findForm")
	@ResponseBody
	public String findForm(Worker worker,HttpServletRequest request){
		Integer result = 0;
		result = workerService.findWorker(worker);
		if(result>0){
			return "success";
		}else{
			return "fail1";
		}
	}
	

}

