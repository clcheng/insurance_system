package com.zyy.controller;


import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zyy.entity.Admin;
import com.zyy.entity.Retired;
import com.zyy.entity.Total;
import com.zyy.entity.Worker;
import com.zyy.service.AdminService;
import com.zyy.service.TotalService;
import com.zyy.service.WorkerService;
import com.zyy.utils.Layui;
import com.zyy.utils.MD5Util;
import com.zyy.utils.PageUtils;

@Controller
@RequestMapping("admin")
public class AdminController {
	@Resource
	AdminService adminService;
	@Resource
	TotalService totalService;
	@Resource
	WorkerService workerService;
	@RequestMapping("/login")
	public String login(Admin admin,Model model,HttpServletRequest request){
		if(admin.getUserid().length()!=6){
			model.addAttribute("error", "密码长度只能为6");
			return "toLogin";
		}else if(admin.getPassword().length()>18||admin.getPassword().length()<6){
			model.addAttribute("error", "密码长度只能为6~18");
			return "toLogin";
		}
		String pass = MD5Util.EncoderPwdByMd5(admin.getPassword());
		admin.setPassword(pass);
		Admin loginAdmin = adminService.findAdmin(admin);
		HttpSession session = request.getSession();
		if(loginAdmin == null){
			model.addAttribute("error", "没有该用户");
			return "toLogin";
		}else{
			session.setAttribute("pri", loginAdmin.getPri());
			Worker worker = workerService.findWorkerNa(loginAdmin.getUsername());
			if(null != worker){
				session.setAttribute("userID", worker.getWork_id());
			}
			return "index";
		}
	}
	
	@RequestMapping("")
	public String index(){
		return "toLogin";
	}
	
	@RequestMapping("add")
	public String add(){
		return "admin/addOperator";
	}
	@RequestMapping("edit")
	public String edit(){
		return "admin/editOperator";
	}
	@RequestMapping("editAdmin")
	public String editAdmin(Model model,HttpServletRequest request){
		Admin admin = adminService.findName(request.getParameter("adminId"));
		model.addAttribute("admin", admin);
		return "admin/editOper";
	}
	
	@RequestMapping("list")
	@ResponseBody
	public Layui list(Model model,@RequestParam Map<String, Object> params){
		List<Admin> adminList = adminService.list();
		int total = adminService.count();
		PageUtils pageUtils = new PageUtils(adminList, total, 10, 1);
		model.addAttribute("admin", adminList);
		return Layui.data(pageUtils.getTotalCount(), pageUtils.getList());
	}
	
	@RequestMapping("delete")
	@ResponseBody
	public String delete(Model model,HttpServletRequest request){
		String userId = request.getParameter("userid");
		Integer result = 0;
		result = adminService.delete(userId);
		if(result>0){
			return "success";
		}else{
			return "fail";
		}
	}
	
	@RequestMapping("addForm")
	@ResponseBody
	public String addForm(@RequestBody Admin admin){
		Integer result = 0;
		admin.setPassword(MD5Util.EncoderPwdByMd5(admin.getPassword()));
		Admin existAdmin = adminService.findAdmin(admin);
		if(existAdmin!=null){
			return "fail1";
		}else{
			result = adminService.addAdmin(admin);
		}
		if(result>0){
			return "success";
		}else{
			return "fail";
		}
	}
	
	@RequestMapping("editForm")
	@ResponseBody
	public String editForm(@RequestBody Admin admin){
		Integer result = 0;
		admin.setPassword(MD5Util.EncoderPwdByMd5(admin.getPassword()));
		result = adminService.editAdmin(admin);
		if(result>0){
			return "success";
		}else{
			return "fail";
		}
	}
	//=======系统参数==============
	@RequestMapping("addParameter")
	public String addParameter(){
		Total total = totalService.findParam();
		if(total != null){
			return "admin/parameterError";
		}else
			return "admin/addParameter";
	}
	@RequestMapping("updateParameter")
	public String updateParameter(Model model){
		Total total = totalService.findParam();
		model.addAttribute("total", total);
		return "admin/updateParameter";
	}
	
	@RequestMapping("addParameterForm")
	@ResponseBody
	public String addParameterForm(@RequestBody Total total){
		Integer result = 0;
		result = totalService.addTotal(total);
		if(result>0){
			return "success";
		}else{
			return "fail";
		}
	}
	@RequestMapping("updateParameterForm")
	@ResponseBody
	public String updateParameterForm(@RequestBody Total total){
		Integer result = 0;
		result = totalService.updateTotal(total);
		if(result>0){
			return "success";
		}else{
			return "fail";
		}
	}

}

