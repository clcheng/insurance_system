package com.zyy.controller;


import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zyy.entity.Compaccount;
import com.zyy.entity.Company;
import com.zyy.entity.Dead;
import com.zyy.entity.Retired;
import com.zyy.entity.Workaccount;
import com.zyy.entity.Worker;
import com.zyy.service.CompanyService;
import com.zyy.service.DeadService;
import com.zyy.service.PayService;
import com.zyy.service.RetiredService;
import com.zyy.service.WorkerService;
import com.zyy.utils.Layui;
import com.zyy.utils.PageUtils;

@Controller
@RequestMapping("findInfo")
public class FindInfoController {
	
	@Resource
	private CompanyService companyService;
	@Resource
	private WorkerService workerService;
	@Resource
	private RetiredService retiredService;
	@Resource
	private DeadService deadService;
	@Resource
	private PayService payService;
	
	@RequestMapping("/payTally")
	public String payTally(Model model,HttpServletRequest request){
		return "findInfo/payFind";
	}
	@RequestMapping("/workpay")
	public String workpay(Model model,HttpServletRequest request){
		return "findInfo/workpay";
	}
	@RequestMapping("/oneFind")
	public String oneFind(Model model,HttpServletRequest request){
		return "findInfo/oneFind";
	}
	@RequestMapping("/monthFind")
	public String monthFind(Model model,HttpServletRequest request){
		return "findInfo/monthFind";
	}
	@RequestMapping("/deadFind")
	public String deadFind(Model model,HttpServletRequest request){
		return "findInfo/deadFind";
	}
	@RequestMapping("/monthTolly")
	public String monthTolly(Model model,HttpServletRequest request){
		model.addAttribute("retir_id", request.getParameter("retired_id"));
		model.addAttribute("retiracc_flag", "0");
		return "findInfo/listTolly";
	}
	@RequestMapping("/oneTolly")
	public String oneTolly(Model model,HttpServletRequest request){
		model.addAttribute("retir_id", request.getParameter("retired_id"));
		model.addAttribute("retiracc_flag", "1");
		return "findInfo/listTolly";
	}
	@RequestMapping("/deadTolly")
	public String deadTolly(Model model,HttpServletRequest request){
		model.addAttribute("retir_id", request.getParameter("retired_id"));
		model.addAttribute("retiracc_flag", "2");
		return "findInfo/listTolly";
	}
	@RequestMapping("payList")
	public String payList(Model model,HttpServletRequest request){
		model.addAttribute("compacc_flag", request.getParameter("flag"));
		model.addAttribute("comp_id", request.getParameter("compId"));
		return "findInfo/list";
	}
	@RequestMapping("comppayList")
	public String comppayList(Model model,HttpServletRequest request){
		model.addAttribute("compacc_flag", request.getParameter("flag"));
		model.addAttribute("comp_id", request.getParameter("compId"));
		return "findInfo/comppayList";
	}
	@RequestMapping("workpayList")
	public String workpayList(Model model,HttpServletRequest request){
		model.addAttribute("workacc_flag", request.getParameter("flag"));
		model.addAttribute("work_id", request.getParameter("work_id"));
		return "findInfo/workpayList";
	}

	@RequestMapping("wplist")
	@ResponseBody
	public Layui wplist(Model model,@RequestParam Map<String, Object> params,HttpServletRequest request){
		String work_id = request.getParameter("cid");
		Workaccount workaccount = new Workaccount();
		workaccount.setWork_id(work_id);
		List<Workaccount> workaccountList = payService.listWorkaccount(workaccount);
		int total = payService.countWorkaccount(workaccount);
		PageUtils pageUtils = new PageUtils(workaccountList, total, 10, 1);
		model.addAttribute("admin", workaccountList);
		return Layui.data(pageUtils.getTotalCount(), pageUtils.getList());
	}
	

	@RequestMapping("/company")
	public String company(Model model){
		return "findInfo/findCompany";
	}
	@RequestMapping("/work")
	public String work(Model model){
		return "findInfo/findWorker";
	}
	@RequestMapping("/retired")
	public String retired(Model model){
		return "findInfo/findRetire";
	}
	@RequestMapping("/dead")
	public String dead(Model model){
		return "findInfo/findDead";
	}
	@RequestMapping("/showComp")
	public String showComp(Model model,HttpServletRequest request){
		String compId = request.getParameter("compId");
		Company company = companyService.findName(compId);
		model.addAttribute("company", company);
		return "findInfo/showComp";
	}
	@RequestMapping("/showWorker")
	public String showWorker(Model model,HttpServletRequest request){
		String workId = request.getParameter("workId");
		Worker worker = workerService.findWorkerName(workId);
		model.addAttribute("worker", worker);
		return "findInfo/showWorker";
	}
	@RequestMapping("/showRetire")
	public String showRetire(HttpServletRequest request,Model model){
		String retiredId = request.getParameter("retiredId");
		Retired retired = retiredService.findRetiredName(retiredId);
		model.addAttribute("retired",retired);
		return "findInfo/showRetire";
	}
	@RequestMapping("/showDead")
	public String showDead(HttpServletRequest request,Model model){
		String deadId = request.getParameter("deadId");
		Dead dead = deadService.findDeadName(deadId);
		model.addAttribute("dead",dead);
		return "findInfo/showDead";
	}
	@RequestMapping("/common")
	public String common(HttpServletRequest request,Model model){
		String workId = (String) request.getSession().getAttribute("userID");
		Worker worker = workerService.findWorkerName(workId);
		model.addAttribute("worker", worker);
		return "common/common_manage";
	}
	@RequestMapping("/message")
	public String message(HttpServletRequest request,Model model){
		model.addAttribute("workacc_flag", "1");
		model.addAttribute("work_id", (String) request.getSession().getAttribute("userID"));
		return "common/common_message";
	}
	@RequestMapping("/select")
	public String select(HttpServletRequest request,Model model){
		model.addAttribute("retir_id", (String) request.getSession().getAttribute("userID"));
		model.addAttribute("retiracc_flag", "0");
		return "common/common_select";
	}
}

