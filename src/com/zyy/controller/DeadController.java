package com.zyy.controller;


import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zyy.entity.Dead;
import com.zyy.service.DeadService;

@Controller
@RequestMapping("dead")
public class DeadController {
	
	@Resource
	private DeadService deadService;
	
	@RequestMapping("/findDead")
	public String findDead(){
		return "dead/findDead";
	}
	
	@RequestMapping("/dead")
	public String retire(HttpServletRequest request,Model model){
		String deadId = request.getParameter("deadId");
		Dead dead = deadService.findDeadName(deadId);
		model.addAttribute("dead",dead);
		return "dead/editDead";
	}
	
	@RequestMapping("/editForm")
	@ResponseBody
	public String editForm(@RequestBody Dead dead){
		Integer result = 0;
		result = deadService.editDead(dead);
		if(result>0){
			return "success";
		}else{
			return "fail";
		}
	}
	
	@RequestMapping("/findForm")
	@ResponseBody
	public String findForm(Dead dead,HttpServletRequest request){
		HttpSession session = request.getSession();
		session.setAttribute("deadId", dead.getDead_id());
		Integer result = 0;
		try {
			result = deadService.findDead(dead);
		} catch (Exception e) {
			return "fail";
		}
		if(result>0){
			return "success";
		}else{
			return "fail1";
		}
	}

}

