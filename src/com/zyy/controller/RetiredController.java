package com.zyy.controller;


import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zyy.entity.Retired;
import com.zyy.service.RetiredService;

@Controller
@RequestMapping("retired")
public class RetiredController {
	
	@Resource
	private RetiredService retiredService;
	
	@RequestMapping("/findRetire")
	public String findRetire(){
		return "retired/findRetire";
	}
	
	@RequestMapping("/findRetireDead")
	public String findRetireDead(){
		return "retired/findRetireDead";
	}
	
	@RequestMapping("/retire")
	public String retire(HttpServletRequest request,Model model){
		String retiredId = request.getParameter("retiredId");
		Retired retired = retiredService.findRetiredName(retiredId);
		model.addAttribute("retired",retired);
		return "retired/editRetire";
	}
		
	@RequestMapping("/retireDead")
	public String retireDead(HttpServletRequest request,Model model){
		String retiredId = request.getParameter("retiredId");
		Retired retired = retiredService.findRetiredName(retiredId);
		model.addAttribute("retired",retired);
		return "retired/retireDead";
	}
	
	@RequestMapping("/editForm")
	@ResponseBody
	public String editForm(@RequestBody Retired retired){
		Integer result = 0;
		try {
			result = retiredService.editRetired(retired);
		} catch (Exception e) {
			return "fail1";
		}
		if(result>0){
			return "success";
		}else{
			return "fail";
		}
	}
	
	@RequestMapping("/findForm")
	@ResponseBody
	public String findForm(Retired retired,HttpServletRequest request){
		HttpSession session = request.getSession();
		session.setAttribute("retiredId", retired.getRetired_id());
		Integer result = 0;
		try {
			result = retiredService.findRetired(retired);
		} catch (Exception e) {
			return "fail";
		}
		if(result>0){
			return "success";
		}else{
			return "fail1";
		}
	}

}

