package com.zyy.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zyy.entity.Compaccount;
import com.zyy.entity.Dead;
import com.zyy.entity.Retiraccount;
import com.zyy.entity.Retired;
import com.zyy.entity.Total;
import com.zyy.service.DeadService;
import com.zyy.service.GrantService;
import com.zyy.service.RetiredService;
import com.zyy.service.TotalService;
import com.zyy.utils.Layui;
import com.zyy.utils.PageUtils;

@Controller
@RequestMapping("grant")
public class GrantController {
	@Resource
	private GrantService grantService;
	@Resource
	private RetiredService retiredService;
	@Resource
	private TotalService totalService;
	@Resource
	private DeadService deadService;
	
	@RequestMapping("/monthCount")
	public String monthCount(Model model,HttpServletRequest request){
		return "grant/monthCount";
	}
	@RequestMapping("/oneCount")
	public String oneCount(Model model,HttpServletRequest request){
		return "grant/oneCount";
	}
	@RequestMapping("/deadCount")
	public String deadCount(Model model,HttpServletRequest request){
		return "grant/deadCount";
	}
	@RequestMapping("/oneFind")
	public String oneFind(Model model,HttpServletRequest request){
		return "grant/oneFind";
	}
	@RequestMapping("/monthFind")
	public String monthFind(Model model,HttpServletRequest request){
		return "grant/monthFind";
	}
	@RequestMapping("/deadFind")
	public String deadFind(Model model,HttpServletRequest request){
		return "grant/deadFind";
	}
	@RequestMapping("/monthTolly")
	public String monthTolly(Model model,HttpServletRequest request){
		model.addAttribute("retir_id", request.getParameter("retired_id"));
		model.addAttribute("retiracc_flag", "0");
		return "grant/listTolly";
	}
	@RequestMapping("/oneTolly")
	public String oneTolly(Model model,HttpServletRequest request){
		model.addAttribute("retir_id", request.getParameter("retired_id"));
		model.addAttribute("retiracc_flag", "1");
		return "grant/listTolly";
	}
	@RequestMapping("/deadTolly")
	public String deadTolly(Model model,HttpServletRequest request){
		model.addAttribute("retir_id", request.getParameter("dead_id"));
		model.addAttribute("retiracc_flag", "2");
		return "grant/listTolly";
	}
	
	/**
	 * 查找十年以上的退休人员，即按月发放
	 * @param request
	 * @param compaccount
	 * @return
	 */
	@RequestMapping("/findTenForm")
	@ResponseBody
	public String findTenForm(HttpServletRequest request){
		Integer result = 0;
		String retired_id = request.getParameter("retired_id");
		try {
			result = grantService.findTenForm(retired_id);
		} catch (Exception e) {
			return "fail1";
		}
		if(result>0){
			return "success";
		}else{
			return "fail1";
		}
	}
	@RequestMapping("/findForm")
	@ResponseBody
	public String findForm(HttpServletRequest request){
		String retired_id = request.getParameter("retired_id");
		Integer result = 0;
		try {
			result = grantService.findForm(retired_id);
		} catch (Exception e) {
			return "fail1";
		}
		if(result>0){
			return "success";
		}else{
			return "fail1";
		}
	}
	@RequestMapping("/findDeadForm")
	@ResponseBody
	public String findDeadForm(HttpServletRequest request){
		String dead_id = request.getParameter("dead_id");
		Integer result = 0;
		try {
			result = grantService.findForm(dead_id);
		} catch (Exception e) {
			return "fail1";
		}
		if(result>0){
			return "success";
		}else{
			return "fail1";
		}
	}
	
	@RequestMapping("/monthResult")
	public String monthResult(HttpServletRequest request,Model model){
		String retired_id = request.getParameter("retired_id");
		Retired retired = retiredService.findRetiredName(retired_id);
		Retiraccount retiraccount = new Retiraccount();
		retiraccount.setRetir_id(retired_id);
		retiraccount.setRetiracc_date(new Date());
		retiraccount.setComp_id(retired.getComp_id());
		retiraccount.setRetiracc_flag(0);
		Total total = totalService.findParam();
		float retiracc_money = (float) (total.getAveragesalary()*0.2+retired.getRetired_account()/120);
		retiraccount.setRetiracc_money(retiracc_money);
		model.addAttribute("retiraccount", retiraccount);
		grantService.insert(retiraccount);
		return "grant/monthResult";
	}
	
	@RequestMapping("/oneResult")
	public String oneResult(HttpServletRequest request,Model model){
		String retired_id = request.getParameter("retired_id");
		Retired retired = retiredService.findRetiredName(retired_id);
		Retiraccount retiraccount = new Retiraccount();
		retiraccount.setRetir_id(retired_id);
		retiraccount.setRetiracc_date(new Date());
		retiraccount.setComp_id(retired.getComp_id());
		retiraccount.setRetiracc_flag(2);
		Total total = totalService.findParam();
		float retiracc_money = (float) (total.getAveragesalary()*2+retired.getRetired_account());
		retiraccount.setRetiracc_money(retiracc_money);
		model.addAttribute("retiraccount", retiraccount);
		grantService.insert(retiraccount);
		return "grant/oneResult";
	}
	@RequestMapping("/deadResult")
	public String deadResult(HttpServletRequest request,Model model){
		String dead_id = request.getParameter("dead_id");
		Dead dead = deadService.findDeadName(dead_id);
		Retiraccount retiraccount = new Retiraccount();
		retiraccount.setRetir_id(dead_id);
		retiraccount.setRetiracc_date(new Date());
		retiraccount.setComp_id(dead.getComp_id());
		retiraccount.setRetiracc_flag(2);
		Total total = totalService.findParam();
		float retiracc_money = (float) (total.getAveragesalary()*2+dead.getDead_account());
		retiraccount.setRetiracc_money(retiracc_money);
		model.addAttribute("retiraccount", retiraccount);
		grantService.insert(retiraccount);
		return "grant/oneResult";
	}
	
	@RequestMapping("list")
	@ResponseBody
	public Layui list(Model model,@RequestParam Map<String, Object> params,HttpServletRequest request){
		String retir_id = request.getParameter("cid");
		String retiracc_flag = request.getParameter("flag");
		Retiraccount retiraccount = new Retiraccount();
		retiraccount.setRetir_id(retir_id);
		retiraccount.setRetiracc_flag(Integer.valueOf(retiracc_flag));
		List<Retiraccount> retiraccountList = grantService.listRetiraccount(retiraccount);
		int total = grantService.countRetiraccount(retiraccount);
		PageUtils pageUtils = new PageUtils(retiraccountList, total, 10, 1);
		model.addAttribute("admin", retiraccountList);
		return Layui.data(pageUtils.getTotalCount(), pageUtils.getList());
	}
}

