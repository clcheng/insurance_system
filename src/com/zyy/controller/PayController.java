package com.zyy.controller;


import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zyy.entity.Admin;
import com.zyy.entity.Compaccount;
import com.zyy.entity.CompaccountDown;
import com.zyy.entity.Total;
import com.zyy.entity.Workaccount;
import com.zyy.service.CompanyService;
import com.zyy.service.PayService;
import com.zyy.service.TotalService;
import com.zyy.service.WorkerService;
import com.zyy.utils.FormatDate;
import com.zyy.utils.Layui;
import com.zyy.utils.PageUtils;

@Controller
@RequestMapping("pay")
public class PayController {
	@Resource
	PayService payService;
	@Resource
	CompanyService companyService;
	@Resource
	WorkerService workerService;
	@Resource
	TotalService totalService;
	
	@RequestMapping("/addWage")
	public String addWage(Model model,HttpServletRequest request){
		return "pay/addWage";
	}
	@RequestMapping("/payCount")
	public String payCount(Model model,HttpServletRequest request){
		return "pay/payCount";
	}
	@RequestMapping("/payTally")
	public String payTally(Model model,HttpServletRequest request){
		return "pay/payFind";
	}
	@RequestMapping("/payAfter")
	public String payAfter(Model model,HttpServletRequest request){
		return "pay/payAfterFind";
	}
	@RequestMapping("/findForm")
	@ResponseBody
	public String findForm(HttpServletRequest request,Compaccount compaccount){
		Integer result = 0;
			result = payService.findCompany(compaccount);
		if(result>0){
			return "success";
		}else{
			return "fail1";
		}
	}

	@RequestMapping("addForm")
	@ResponseBody
	public String addForm(@RequestBody Workaccount workaccount){
		Integer result = 0;
		if(workerService.findWorkAndComp(workaccount.getWork_id(),workaccount.getComp_id())==null){
			return "fail2";
		}
		workaccount.setWorkacc_date(new Date());
		Total total = totalService.findParam();
		//工资小于平均工资的60%
		if(workaccount.getWork_salary()<=total.getAveragesalary()*0.6){
			workaccount.setWorkacc_salary((float) (total.getAveragesalary()*0.6));
		}else if(workaccount.getWork_salary()>=total.getAveragesalary()*3){
			workaccount.setWorkacc_salary((float) (total.getAveragesalary()*3));
		}else{
			workaccount.setWorkacc_salary(workaccount.getWork_salary());
		}
		float workaccMoney = workaccount.getWorkacc_salary()*total.getWork_ratio();
		float compaccMoney = (float) (workaccount.getWorkacc_salary()*(0.28-total.getWork_ratio()));
		float wcMoney = workaccMoney + compaccMoney;
		workaccount.setWorkacc_money(workaccMoney);
		workaccount.setCompacc_money(compaccMoney);
		//加入员工个人账户
		workerService.updateAccount(workaccount.getWork_id(),wcMoney);
		result = payService.addWage(workaccount);
		if(result>0){
			return "success";
		}else{
			return "fail";
		}
	}
	
	@RequestMapping("countForm")
	public String countForm(Model model,HttpServletRequest request) throws ParseException{
		String compId = request.getParameter("compId");
		Integer result = payService.findCompaccountByDate(compId,FormatDate.cfirstDay(),FormatDate.clastDay());
		if(result == 0){
			//进行计算
			CompaccountDown down = payService.countMoney(compId);
			down.setCompany(compId);
			down.setCompaccDate(new Date());
			if(FormatDate.currentDay()<=15){
				payService.insertCompAccount(down);
			}else{
				down.setCompaccFlag(0);
				down.setCompaccLaterMoney(FormatDate.differenceDay(new Date())*totalService.findParam().getComp_lateratio()*(down.getCompaccMoneySum()+down.getWorkaccMoneySum()));
				payService.insertCompAccount(down);
			}
		}
		Compaccount compaccount = payService.findCompaccount(compId).get(0);
		model.addAttribute("compaccount", compaccount);
		return "pay/countForm";
	}
	

	@RequestMapping("payList")
	public String payList(Model model,HttpServletRequest request){
		model.addAttribute("compacc_flag", request.getParameter("flag"));
		model.addAttribute("comp_id", request.getParameter("compId"));
		return "pay/list";
	}
	
	@RequestMapping("list")
	@ResponseBody
	public Layui list(Model model,@RequestParam Map<String, Object> params,HttpServletRequest request){
		String comp_id = request.getParameter("cid");
		Integer limit = Integer.valueOf(request.getParameter("limit"));
		Integer curr = Integer.valueOf(request.getParameter("page"));
		Compaccount compaccount = new Compaccount();
		compaccount.setComp_id(comp_id);
		if(request.getParameter("flag")!=null && request.getParameter("flag") != ""){
			String compacc_flag = request.getParameter("flag");
			compaccount.setCompacc_flag(Integer.valueOf(compacc_flag));
		}
		List<Compaccount> compaccountList = new ArrayList<Compaccount>();
		for (Compaccount compaccount2 : payService.listCompaccount(compaccount)) {
			compaccount2.setCompacc_dateStr(FormatDate.dateTime(compaccount2.getCompacc_date()));
			compaccountList.add(compaccount2);
		}
//				payService.listCompaccount(compaccount);
		int total = payService.countCompaccount(compaccount);
		PageUtils pageUtils = new PageUtils(compaccountList, total, limit, curr);
		model.addAttribute("admin", pageUtils.getList());
		return Layui.data(pageUtils.getTotalCount(), pageUtils.getList());
	}
	
}

