package com.zyy.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * @ClassName: Retired
 * @Description: 退休档案类（，，，，，，，，，，，，，，社会支付，，个人账户，个人账户剩余金额，个人账户支付，）
 */
public class Retired implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	private String retired_id;					//退休职工身份证号
	private String comp_id;					//所在单位
	private String retired_name;					//姓名
	private String retired_telephone;			//电话
	private String retired_address;				//地址
	private String retired_post;					//邮编
	private String retired_sex;					//性别
	private Date retired_birth;					//出生日期
	private String retired_nation;				//民族
	private String retired_worktype;				//用工形式
	private String retired_type;					//职务
	private Date retired_beginwork;				//参加工作时间
	private Date retired_begintime;				//开始参保时间 
	private Date retired_retirtime;				//退休时间
	private int retired_worktime;				//缴费年数
	private String retired_accid;				//账户号
	private float retired_account;           	//本月余额
	private float retired_leftaccount;			//发放后余额
	private int retired_state;					//退休状态
	public String getRetired_id() {
		return retired_id;
	}
	public void setRetired_id(String retired_id) {
		this.retired_id = retired_id;
	}
	public String getComp_id() {
		return comp_id;
	}
	public void setComp_id(String comp_id) {
		this.comp_id = comp_id;
	}
	public String getRetired_name() {
		return retired_name;
	}
	public void setRetired_name(String retired_name) {
		this.retired_name = retired_name;
	}
	public String getRetired_telephone() {
		return retired_telephone;
	}
	public void setRetired_telephone(String retired_telephone) {
		this.retired_telephone = retired_telephone;
	}
	public String getRetired_address() {
		return retired_address;
	}
	public void setRetired_address(String retired_address) {
		this.retired_address = retired_address;
	}
	public String getRetired_post() {
		return retired_post;
	}
	public void setRetired_post(String retired_post) {
		this.retired_post = retired_post;
	}
	public String getRetired_sex() {
		return retired_sex;
	}
	public void setRetired_sex(String retired_sex) {
		this.retired_sex = retired_sex;
	}
	public Date getRetired_birth() {
		return retired_birth;
	}
	public void setRetired_birth(Date retired_birth) {
		this.retired_birth = retired_birth;
	}
	public String getRetired_nation() {
		return retired_nation;
	}
	public void setRetired_nation(String retired_nation) {
		this.retired_nation = retired_nation;
	}
	public String getRetired_worktype() {
		return retired_worktype;
	}
	public void setRetired_worktype(String retired_worktype) {
		this.retired_worktype = retired_worktype;
	}
	public String getRetired_type() {
		return retired_type;
	}
	public void setRetired_type(String retired_type) {
		this.retired_type = retired_type;
	}
	public Date getRetired_beginwork() {
		return retired_beginwork;
	}
	public void setRetired_beginwork(Date retired_beginwork) {
		this.retired_beginwork = retired_beginwork;
	}
	public Date getRetired_begintime() {
		return retired_begintime;
	}
	public void setRetired_begintime(Date retired_begintime) {
		this.retired_begintime = retired_begintime;
	}
	public Date getRetired_retirtime() {
		return retired_retirtime;
	}
	public void setRetired_retirtime(Date retired_retirtime) {
		this.retired_retirtime = retired_retirtime;
	}
	public int getRetired_worktime() {
		return retired_worktime;
	}
	public void setRetired_worktime(int retired_worktime) {
		this.retired_worktime = retired_worktime;
	}
	public String getRetired_accid() {
		return retired_accid;
	}
	public void setRetired_accid(String retired_accid) {
		this.retired_accid = retired_accid;
	}
	public float getRetired_account() {
		return retired_account;
	}
	public void setRetired_account(float retired_account) {
		this.retired_account = retired_account;
	}
	public float getRetired_leftaccount() {
		return retired_leftaccount;
	}
	public void setRetired_leftaccount(float retired_leftaccount) {
		this.retired_leftaccount = retired_leftaccount;
	}
	public int getRetired_state() {
		return retired_state;
	}
	public void setRetired_state(int retired_state) {
		this.retired_state = retired_state;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	

}
