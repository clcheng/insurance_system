package com.zyy.entity;

/**
 * @ClassName: Dead
 * @Description: 死亡档案类
 */
public class Dead implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	private String dead_id;              //死亡人身份证号
	private String comp_id;			    //所在单位 
	private String dead_name;			//姓名
	private String dead_sex;				//性别
	private String dead_accid;			//账户号
	private float dead_account;			//个人账户余额
	private String dead_relaid;			//待遇领取人身份证号
	private String dead_relaname;		//待遇领取人姓名
	private String dead_relasex;			//待遇领取人性别
	private String dead_relaphone;		//待遇领取人电话
	private String dead_relaaddress;		//待遇领取人地址
	private String dead_relapost;		//待遇领取人邮编
	private float dead_help1;			//丧葬补助费
	private float dead_help2;			//抚恤费用
	private int dead_flag;				//发放标志
	public String getDead_id() {
		return dead_id;
	}
	public void setDead_id(String dead_id) {
		this.dead_id = dead_id;
	}
	public String getComp_id() {
		return comp_id;
	}
	public void setComp_id(String comp_id) {
		this.comp_id = comp_id;
	}
	public String getDead_name() {
		return dead_name;
	}
	public void setDead_name(String dead_name) {
		this.dead_name = dead_name;
	}
	public String getDead_sex() {
		return dead_sex;
	}
	public void setDead_sex(String dead_sex) {
		this.dead_sex = dead_sex;
	}
	public String getDead_accid() {
		return dead_accid;
	}
	public void setDead_accid(String dead_accid) {
		this.dead_accid = dead_accid;
	}
	public float getDead_account() {
		return dead_account;
	}
	public void setDead_account(float dead_account) {
		this.dead_account = dead_account;
	}
	public String getDead_relaid() {
		return dead_relaid;
	}
	public void setDead_relaid(String dead_relaid) {
		this.dead_relaid = dead_relaid;
	}
	public String getDead_relaname() {
		return dead_relaname;
	}
	public void setDead_relaname(String dead_relaname) {
		this.dead_relaname = dead_relaname;
	}
	public String getDead_relasex() {
		return dead_relasex;
	}
	public void setDead_relasex(String dead_relasex) {
		this.dead_relasex = dead_relasex;
	}
	public String getDead_relaphone() {
		return dead_relaphone;
	}
	public void setDead_relaphone(String dead_relaphone) {
		this.dead_relaphone = dead_relaphone;
	}
	public String getDead_relaaddress() {
		return dead_relaaddress;
	}
	public void setDead_relaaddress(String dead_relaaddress) {
		this.dead_relaaddress = dead_relaaddress;
	}
	public String getDead_relapost() {
		return dead_relapost;
	}
	public void setDead_relapost(String dead_relapost) {
		this.dead_relapost = dead_relapost;
	}
	public float getDead_help1() {
		return dead_help1;
	}
	public void setDead_help1(float dead_help1) {
		this.dead_help1 = dead_help1;
	}
	public float getDead_help2() {
		return dead_help2;
	}
	public void setDead_help2(float dead_help2) {
		this.dead_help2 = dead_help2;
	}
	public int getDead_flag() {
		return dead_flag;
	}
	public void setDead_flag(int dead_flag) {
		this.dead_flag = dead_flag;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
