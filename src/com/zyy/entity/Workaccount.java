package com.zyy.entity;

import java.util.Date;

/**
 * @ClassName: Workaccount
 * @Description: 在职保险金缴纳记录类
 */
public class Workaccount implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	private int id;					//记录号
	private String comp_id;			//所在单位
	private String work_id;			//在职职工
	private Date workacc_date;		//缴费日期
	private float work_salary;		//本月工资
	private float workacc_salary;	//本月缴费工资基数
	private float workacc_money;		//职工缴费
	private float compacc_money;		//单位缴费
	private int workacc_flag;		//缴费标志
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getComp_id() {
		return comp_id;
	}
	public void setComp_id(String comp_id) {
		this.comp_id = comp_id;
	}
	public String getWork_id() {
		return work_id;
	}
	public void setWork_id(String work_id) {
		this.work_id = work_id;
	}
	public Date getWorkacc_date() {
		return workacc_date;
	}
	public void setWorkacc_date(Date workacc_date) {
		this.workacc_date = workacc_date;
	}
	public float getWork_salary() {
		return work_salary;
	}
	public void setWork_salary(float work_salary) {
		this.work_salary = work_salary;
	}
	public float getWorkacc_salary() {
		return workacc_salary;
	}
	public void setWorkacc_salary(float workacc_salary) {
		this.workacc_salary = workacc_salary;
	}
	public float getWorkacc_money() {
		return workacc_money;
	}
	public void setWorkacc_money(float workacc_money) {
		this.workacc_money = workacc_money;
	}
	public float getCompacc_money() {
		return compacc_money;
	}
	public void setCompacc_money(float compacc_money) {
		this.compacc_money = compacc_money;
	}
	public int getWorkacc_flag() {
		return workacc_flag;
	}
	public void setWorkacc_flag(int workacc_flag) {
		this.workacc_flag = workacc_flag;
	}
	
}
