package com.zyy.entity;

public class Admin {
	private String userid;
	private String username; 
	private String password;
	private Integer pri;
	public Admin() {
		super();
	}
	public Admin(String userid, String username, String password, Integer pri) {
		super();
		this.userid = userid;
		this.username = username;
		this.password = password;
		this.pri = pri;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Integer getPri() {
		return pri;
	}
	public void setPri(Integer pri) {
		this.pri = pri;
	}
	@Override
	public String toString() {
		return "Admin [userid=" + userid + ", username=" + username + ", password=" + password + ", pri=" + pri + "]";
	}
	

}
