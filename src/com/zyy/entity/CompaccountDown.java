package com.zyy.entity;

import java.util.Date;

/**
 * @ClassName: CompaccountDown
 * @Description: 单位保险金缴纳记录下行参数
 */
public class CompaccountDown implements java.io.Serializable {

	private float workaccMoneySum;       //个人缴费总金额
	private float compaccMoneySum;       //公司缴费总金额
	private String company;
	private Date compaccDate;
	private float compaccLaterMoney = 0;       //公司延迟缴费总金额
	private Integer compaccFlag = 1;
	public float getWorkaccMoneySum() {
		return workaccMoneySum;
	}
	public void setWorkaccMoneySum(float workaccMoneySum) {
		this.workaccMoneySum = workaccMoneySum;
	}
	public float getCompaccMoneySum() {
		return compaccMoneySum;
	}
	public void setCompaccMoneySum(float compaccMoneySum) {
		this.compaccMoneySum = compaccMoneySum;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public Date getCompaccDate() {
		return compaccDate;
	}
	public void setCompaccDate(Date compaccDate) {
		this.compaccDate = compaccDate;
	}
	public float getCompaccLaterMoney() {
		return compaccLaterMoney;
	}
	public void setCompaccLaterMoney(float compaccLaterMoney) {
		this.compaccLaterMoney = compaccLaterMoney;
	}
	public Integer getCompaccFlag() {
		return compaccFlag;
	}
	public void setCompaccFlag(Integer compaccFlag) {
		this.compaccFlag = compaccFlag;
	}
	
}
