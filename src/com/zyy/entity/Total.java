package com.zyy.entity;

/**
 * @ClassName: Total
 * @Description: 参数类
 */
public class Total implements java.io.Serializable {
	
	private String year;			//年度
	private float averagesalary;	//上一年月平均工资
	private float totalmoney;		//社会统筹基金
	private float work_ratio;		//个人缴费比例
	private float workacc_ratio;	//划入个人账户比例
	private float comp_lateratio;	//单位补缴滞纳金比例
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public float getAveragesalary() {
		return averagesalary;
	}
	public void setAveragesalary(float averagesalary) {
		this.averagesalary = averagesalary;
	}
	public float getTotalmoney() {
		return totalmoney;
	}
	public void setTotalmoney(float totalmoney) {
		this.totalmoney = totalmoney;
	}
	public float getWork_ratio() {
		return work_ratio;
	}
	public void setWork_ratio(float work_ratio) {
		this.work_ratio = work_ratio;
	}
	public float getWorkacc_ratio() {
		return workacc_ratio;
	}
	public void setWorkacc_ratio(float workacc_ratio) {
		this.workacc_ratio = workacc_ratio;
	}
	public float getComp_lateratio() {
		return comp_lateratio;
	}
	public void setComp_lateratio(float comp_lateratio) {
		this.comp_lateratio = comp_lateratio;
	}

}
