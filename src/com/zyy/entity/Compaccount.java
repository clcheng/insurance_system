package com.zyy.entity;

import java.util.Date;

/**
 * @ClassName: Compaccount
 * @Description: 单位保险金缴纳记录类
 */
public class Compaccount implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	private int id;                        //记录号
	private String comp_id;               //公司
	private Date compacc_date;              //缴费日期
	private String compacc_dateStr;              //缴费日期
	private float workacc_totalmoney;       //个人缴费总金额
	private float compacc_totalmoney;       //单位缴费总金额
	private float compacc_latermoney;       //单位补缴费用
	private int compacc_flag;               //缴费标志 0未交费；1已缴费
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getComp_id() {
		return comp_id;
	}
	public void setComp_id(String comp_id) {
		this.comp_id = comp_id;
	}
	public Date getCompacc_date() {
		return compacc_date;
	}
	public void setCompacc_date(Date compacc_date) {
		this.compacc_date = compacc_date;
	}
	public float getWorkacc_totalmoney() {
		return workacc_totalmoney;
	}
	public void setWorkacc_totalmoney(float workacc_totalmoney) {
		this.workacc_totalmoney = workacc_totalmoney;
	}
	public float getCompacc_totalmoney() {
		return compacc_totalmoney;
	}
	public void setCompacc_totalmoney(float compacc_totalmoney) {
		this.compacc_totalmoney = compacc_totalmoney;
	}
	public float getCompacc_latermoney() {
		return compacc_latermoney;
	}
	public void setCompacc_latermoney(float compacc_latermoney) {
		this.compacc_latermoney = compacc_latermoney;
	}
	public int getCompacc_flag() {
		return compacc_flag;
	}
	public void setCompacc_flag(int compacc_flag) {
		this.compacc_flag = compacc_flag;
	}
	public String getCompacc_dateStr() {
		return compacc_dateStr;
	}
	public void setCompacc_dateStr(String compacc_dateStr) {
		this.compacc_dateStr = compacc_dateStr;
	}

}
