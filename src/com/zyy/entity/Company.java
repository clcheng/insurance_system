package com.zyy.entity;

import java.math.BigDecimal;
import java.util.Date;

public class Company implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	private String comp_id;                       //单位代号
	private String comp_name;					  //单位名称
	private String comp_phone;					  //单位电话
	private String comp_address;				  //单位地址
	private String comp_post;					  //单位邮编
	private String comp_law;					  //法定代表
	private String comp_lawid;					  //法定代表证件号
	private String comp_type;				   	  //单位类型
	private String comp_piece;					  //单位所在区县 
	private String comp_accid;					  //单位账户号
	private float comp_leftaccount;				  //单位账户剩余
	private Date com_date;						  //单位参保日期
	private float com_ratio;					  //缴费比率
	private int comp_state;	                      //单位状态
	public String getComp_id() {
		return comp_id;
	}
	public void setComp_id(String comp_id) {
		this.comp_id = comp_id;
	}
	public String getComp_name() {
		return comp_name;
	}
	public void setComp_name(String comp_name) {
		this.comp_name = comp_name;
	}
	public String getComp_phone() {
		return comp_phone;
	}
	public void setComp_phone(String comp_phone) {
		this.comp_phone = comp_phone;
	}
	public String getComp_address() {
		return comp_address;
	}
	public void setComp_address(String comp_address) {
		this.comp_address = comp_address;
	}
	public String getComp_post() {
		return comp_post;
	}
	public void setComp_post(String comp_post) {
		this.comp_post = comp_post;
	}
	public String getComp_law() {
		return comp_law;
	}
	public void setComp_law(String comp_law) {
		this.comp_law = comp_law;
	}
	public String getComp_lawid() {
		return comp_lawid;
	}
	public void setComp_lawid(String comp_lawid) {
		this.comp_lawid = comp_lawid;
	}
	public String getComp_type() {
		return comp_type;
	}
	public void setComp_type(String comp_type) {
		this.comp_type = comp_type;
	}
	public String getComp_piece() {
		return comp_piece;
	}
	public void setComp_piece(String comp_piece) {
		this.comp_piece = comp_piece;
	}
	public String getComp_accid() {
		return comp_accid;
	}
	public void setComp_accid(String comp_accid) {
		this.comp_accid = comp_accid;
	}
	public float getComp_leftaccount() {
		return comp_leftaccount;
	}
	public void setComp_leftaccount(float comp_leftaccount) {
		this.comp_leftaccount = comp_leftaccount;
	}
	public Date getCom_date() {
		return com_date;
	}
	public void setCom_date(Date com_date) {
		this.com_date = com_date;
	}
	public float getCom_ratio() {
		return com_ratio;
	}
	public void setCom_ratio(float com_ratio) {
		this.com_ratio = com_ratio;
	}
	public int getComp_state() {
		return comp_state;
	}
	public void setComp_state(int comp_state) {
		this.comp_state = comp_state;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
