package com.zyy.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
public class Worker implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private String work_id; // 在职职工身份证号
	private String comp_id; // 所在单位
	private String work_name; // 姓名
	private String work_phone; // 电话
	private String work_address; // 地址
	private String work_post; // 邮编
	private String work_sex; // 性别
	private Date work_birth; // 出生日期
	private String work_nation; // 民族
	private String work_worktype; // 用工形式
	private String work_type; // 职务
	private Date work_beginwork; // 参加工作时间
	private Date work_begintime; // 参保日期
	private String work_accid; // 账户号
	private float work_account; // 个人账户
	private int work_state; // 在职状态
	public String getWork_id() {
		return work_id;
	}
	public void setWork_id(String work_id) {
		this.work_id = work_id;
	}
	public String getComp_id() {
		return comp_id;
	}
	public void setComp_id(String comp_id) {
		this.comp_id = comp_id;
	}
	public String getWork_name() {
		return work_name;
	}
	public void setWork_name(String work_name) {
		this.work_name = work_name;
	}
	public String getWork_phone() {
		return work_phone;
	}
	public void setWork_phone(String work_phone) {
		this.work_phone = work_phone;
	}
	public String getWork_address() {
		return work_address;
	}
	public void setWork_address(String work_address) {
		this.work_address = work_address;
	}
	public String getWork_post() {
		return work_post;
	}
	public void setWork_post(String work_post) {
		this.work_post = work_post;
	}
	public String getWork_sex() {
		return work_sex;
	}
	public void setWork_sex(String work_sex) {
		this.work_sex = work_sex;
	}
	public Date getWork_birth() {
		return work_birth;
	}
	public void setWork_birth(Date work_birth) {
		this.work_birth = work_birth;
	}
	public String getWork_nation() {
		return work_nation;
	}
	public void setWork_nation(String work_nation) {
		this.work_nation = work_nation;
	}
	public String getWork_worktype() {
		return work_worktype;
	}
	public void setWork_worktype(String work_worktype) {
		this.work_worktype = work_worktype;
	}
	public String getWork_type() {
		return work_type;
	}
	public void setWork_type(String work_type) {
		this.work_type = work_type;
	}
	public Date getWork_beginwork() {
		return work_beginwork;
	}
	public void setWork_beginwork(Date work_beginwork) {
		this.work_beginwork = work_beginwork;
	}
	public Date getWork_begintime() {
		return work_begintime;
	}
	public void setWork_begintime(Date work_begintime) {
		this.work_begintime = work_begintime;
	}
	public String getWork_accid() {
		return work_accid;
	}
	public void setWork_accid(String work_accid) {
		this.work_accid = work_accid;
	}
	public float getWork_account() {
		return work_account;
	}
	public void setWork_account(float work_account) {
		this.work_account = work_account;
	}
	public int getWork_state() {
		return work_state;
	}
	public void setWork_state(int work_state) {
		this.work_state = work_state;
	}

}
