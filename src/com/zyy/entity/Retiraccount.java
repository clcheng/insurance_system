package com.zyy.entity;

import java.util.Date;

/**
 * @ClassName: Retiraccount
 * @Description: 退休养老金发放记录类
 */
public class Retiraccount implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	private int id;					//记录号
	private String retir_id;		//退休职工
	private Date retiracc_date;		//发放日期
	private String comp_id;			//所在公司
	private float retiracc_money;	//本月发放养老金总金额
	private int retiracc_flag;		//发放标志
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getRetir_id() {
		return retir_id;
	}
	public void setRetir_id(String retir_id) {
		this.retir_id = retir_id;
	}
	public Date getRetiracc_date() {
		return retiracc_date;
	}
	public void setRetiracc_date(Date retiracc_date) {
		this.retiracc_date = retiracc_date;
	}
	public String getComp_id() {
		return comp_id;
	}
	public void setComp_id(String comp_id) {
		this.comp_id = comp_id;
	}
	public float getRetiracc_money() {
		return retiracc_money;
	}
	public void setRetiracc_money(float retiracc_money) {
		this.retiracc_money = retiracc_money;
	}
	public int getRetiracc_flag() {
		return retiracc_flag;
	}
	public void setRetiracc_flag(int retiracc_flag) {
		this.retiracc_flag = retiracc_flag;
	}

}
