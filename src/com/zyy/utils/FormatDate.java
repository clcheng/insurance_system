package com.zyy.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class FormatDate {
	public static String firstDay(){
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd"); 
		//获取前月的第一天
		Calendar  cal_1=Calendar.getInstance();//获取当前日期 
		cal_1.add(Calendar.MONTH, -1);
		cal_1.set(Calendar.DAY_OF_MONTH,1);//设置为1号,当前日期既为本月第一天 
		return format.format(cal_1.getTime());
	}
	public static String lastDay(){
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd"); 
		//获取前月的最后一天
		Calendar cale = Calendar.getInstance();   
		cale.set(Calendar.DAY_OF_MONTH,0);
		return format.format(cale.getTime());
	}
	public static String cfirstDay(){
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd"); 
		//获取当月的第一天
		Calendar  cal_1=Calendar.getInstance();//获取当前日期 
		cal_1.add(Calendar.MONTH, 0);
		cal_1.set(Calendar.DAY_OF_MONTH,1);//设置为1号,当前日期既为本月第一天 
		return format.format(cal_1.getTime());
	}
	public static String clastDay(){
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd"); 
		//获取当月的最后一天
		Calendar cale = Calendar.getInstance();   
		cale.set(Calendar.DAY_OF_MONTH,cale.getActualMaximum(Calendar.DAY_OF_MONTH));
		return format.format(cale.getTime());
	}
	//时间差（天数）
	public static int differenceDay(Date date) throws ParseException{
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Calendar  cal_1=Calendar.getInstance();//获取当前日期 
		cal_1.add(Calendar.MONTH, 0);
		cal_1.set(Calendar.DAY_OF_MONTH,15);//设置为1号,当前日期既为本月第一天 
		String fromDate = format.format(cal_1.getTime());
		String toDate = format.format(date);  
		long from = format.parse(fromDate).getTime();  
		long to = format.parse(toDate).getTime();  
		return (int) Math.abs((to - from)/(1000 * 60 * 60 * 24)); 
	}
	//当月的第几天
	public static int currentDay(){
		Date date=new Date();  
        Calendar ca=Calendar.getInstance();  
        ca.setTime(date);  
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");  
          
        String st=sdf.format(date);  
        return ca.get(Calendar.DAY_OF_MONTH);  
	}
	
	public static String dateTime(Date time1){
		SimpleDateFormat time=new SimpleDateFormat("yyyy年MM月dd日"); 
		return time.format(time1);
	}
}
